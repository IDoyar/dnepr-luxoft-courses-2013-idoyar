SELECT DISTINCT makers.maker_name FROM product
LEFT JOIN makers ON product.maker_id = makers.maker_id
WHERE type='PC' AND maker_name NOT IN
(SELECT DISTINCT makers.maker_name FROM product
LEFT JOIN makers ON product.maker_id = makers.maker_id
WHERE type='laptop')
