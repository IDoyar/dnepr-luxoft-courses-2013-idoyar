SELECT maker_id, AVG(screen) AS 'avg_size' FROM laptop
LEFT JOIN product ON laptop.model=product.model
GROUP BY maker_id ORDER BY AVG(screen)