SELECT p.model FROM (((product p LEFT JOIN PC pc ON p.model = pc.model)
LEFT JOIN laptop l ON p.model = l.model)
LEFT JOIN printer pr ON p.model = pr.model)
WHERE COALESCE(pc.price, l.price, pr.price) IN
(SELECT MAX(COALESCE(pc.price, l.price, pr.price)) FROM
(((product p LEFT JOIN PC pc ON p.model = pc.model)
LEFT JOIN laptop l ON p.model = l.model)
LEFT JOIN printer pr ON p.model = pr.model)
)