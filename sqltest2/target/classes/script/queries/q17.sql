SELECT DISTINCT maker_id FROM product
LEFT JOIN pc ON product.model=pc.model
WHERE maker_id IN (SELECT DISTINCT maker_id FROM product WHERE type='printer')
AND type='pc' AND speed=(SELECT Max(speed) FROM pc
WHERE ram=(SELECT Min(ram) FROM pc))