SELECT DISTINCT p.model, COALESCE(pc.price, l.price, pr.price) AS 'price'
FROM ((((
product p LEFT JOIN PC pc ON p.model = pc.model)
LEFT JOIN laptop l ON p.model = l.model)
LEFT JOIN printer pr ON p.model = pr.model)
LEFT JOIN makers m ON p.maker_id = m.maker_id)
WHERE m.maker_name = 'B'