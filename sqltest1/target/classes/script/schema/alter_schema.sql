
CREATE TABLE IF NOT EXISTS makers (
	maker_id INT AUTO_INCREMENT,
	maker_name VARCHAR(50) NOT NULL,
	maker_adress VARCHAR(200),
	CONSTRAINT pk_makers PRIMARY KEY(maker_id)
);

INSERT INTO makers(maker_name, maker_adress) VALUES ('A', 'AdressA');
INSERT INTO makers(maker_name, maker_adress) VALUES ('B', 'AdressB');
INSERT INTO makers(maker_name, maker_adress) VALUES ('C', 'AdressC');
INSERT INTO makers(maker_name, maker_adress) VALUES ('D', 'AdressD');
INSERT INTO makers(maker_name, maker_adress) VALUES ('E', 'AdressE');

ALTER TABLE product ADD maker_id INT NOT NULL;
UPDATE product pr SET maker_id = (SELECT maker_id FROM makers mk WHERE pr.maker = mk.maker_name);
ALTER TABLE product DROP COLUMN maker;
ALTER TABLE product ADD CONSTRAINT fk_product_makers FOREIGN KEY(maker_id) REFERENCES makers(maker_id);

CREATE TABLE IF NOT EXISTS printer_type (
	type_id INT,
	type_name VARCHAR(50) NOT NULL,
	CONSTRAINT pk_printer_type PRIMARY KEY(type_id)
);

INSERT INTO printer_type(type_id, type_name) VALUES (1, 'Laser');
INSERT INTO printer_type(type_id, type_name) VALUES (2, 'Jet');
INSERT INTO printer_type(type_id, type_name) VALUES (3, 'Matrix');

ALTER TABLE printer ADD COLUMN type_id INT NOT NULL;
UPDATE printer pr SET type_id = (SELECT type_id FROM printer_type prt WHERE pr.type = prt.type_name);
ALTER TABLE printer DROP COLUMN type;
ALTER TABLE printer ADD CONSTRAINT fk_printer_type FOREIGN KEY(type_id) REFERENCES printer_type(type_id);

ALTER TABLE printer CHANGE color color CHAR(1) NOT NULL;
ALTER TABLE printer ALTER COLUMN color SET DEFAULT 'y';

CREATE INDEX ind_pc_price ON pc(price);
CREATE INDEX ind_laptop_price ON laptop(price);
CREATE INDEX ind_printer_price ON printer (price);



