package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();

        assertTrue(!(book.getPublicationDate() == cloned.getPublicationDate()));
        assertTrue(book.getName() == cloned.getName());
        assertTrue(book.getPrice() == cloned.getPrice());
        assertTrue(book.getCode() == cloned.getCode());

        assertSame(book.getName(),cloned.getName());
        assertSame(book.getCode(),cloned.getCode());
        assertNotSame(book.getPublicationDate(),cloned.getPublicationDate());

    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();

        assertTrue(book.equals(cloned));

        Book book1 = productFactory.createBook("code", "Thinking", 200, new GregorianCalendar(2006, 0, 1).getTime());

        Book book2 = productFactory.createBook("code", "Thinking in Java", 400, new GregorianCalendar(2006, 0, 1).getTime());

        assertTrue(!book.equals(book1));

        assertTrue(book.equals(book2));

    }
}
