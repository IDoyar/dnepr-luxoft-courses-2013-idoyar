package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class BankTest {

    @Test
    public void testMakeMoneyTransaction() throws Exception {

        Bank bank = new Bank();

        Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();

        User user1 = new User();

        WalletInterface wallet1 = new Wallet();

        wallet1.setAmount(BigDecimal.valueOf(1000.123));

        wallet1.setStatus(WalletStatus.BLOCKED);

        wallet1.setMaxAmount(BigDecimal.valueOf(2000));

        user1.setName("Anton");

        user1.setId(1234l);

        user1.setWallet(wallet1);


        User user2 = new User();

        WalletInterface wallet2 = new Wallet();

        wallet2.setAmount(BigDecimal.valueOf(1560));

        wallet2.setStatus(WalletStatus.ACTIVE);

        wallet2.setMaxAmount(BigDecimal.valueOf(1700));

        user2.setName("Vanya");

        user2.setId(1235l);

        user2.setWallet(wallet2);


        users.put(user1.getId(), user1);

        users.put(user2.getId(), user2);


        bank.setUsers(users);

        BigDecimal amount = new BigDecimal(100);

        try {
            bank.makeMoneyTransaction(user1.getId(), user2.getId(), amount);
        } catch (TransactionException e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException: User 'Anton' wallet is blocked");
        }

        wallet1.setStatus(WalletStatus.ACTIVE);

        bank.makeMoneyTransaction(user1.getId(), user2.getId(), amount);

        assertEquals(BigDecimal.valueOf(900.123), user1.getWallet().getAmount());
        assertEquals(BigDecimal.valueOf(1660), user2.getWallet().getAmount());

        wallet2.setStatus(WalletStatus.BLOCKED);

        try {
            bank.makeMoneyTransaction(user1.getId(), user2.getId(), amount);
        } catch (TransactionException e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException: User 'Vanya' wallet is blocked");
        }

        User user3 = new User();

        user3.setName("Vanya");

        user3.setId(1236l);

        try {
            bank.makeMoneyTransaction(user1.getId(), user3.getId(), amount);
        } catch (NoUserFoundException e) {

            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException: User " + user3.getId() + " not found");
        }
        wallet2.setStatus(WalletStatus.ACTIVE);
        wallet1.setAmount(BigDecimal.valueOf(50.3467));

        try {
            bank.makeMoneyTransaction(user1.getId(), user2.getId(), amount);
        } catch (TransactionException e) {

            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException: User 'Anton' has insufficient funds (50.35 < 100.00)");
        }

        wallet2.setMaxAmount(BigDecimal.valueOf(1600));
        wallet1.setAmount(BigDecimal.valueOf(1000));
        wallet2.setAmount(BigDecimal.valueOf(1560));

        try {
            bank.makeMoneyTransaction(user1.getId(), user2.getId(), amount);
        } catch (TransactionException e) {

            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException: User 'Vanya' wallet limit exceeded (1560.00 + 100.00 > 1600.00)");
        }

        Bank bank1 = new Bank("1.7.0_25");

        try {

            Bank bank2 = new Bank("1.7.0_24");
        } catch (IllegalJavaVersionError e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError: Wrong JavaVersion");
        }

    }
}
