//package com.luxoft.dnepr.courses.regular.unit5;
//
//import com.luxoft.dnepr.courses.regular.unit5.dao.AbstractDao;
//import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
//import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
//import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
//
//import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
//
//import org.junit.Test;
//
//import static org.junit.Assert.*;
//
//public class RedisDaoImplTest {
//    @Test
//    public void saveTest() throws UserAlreadyExist {
//        AbstractDao<Redis> redisAbstractDao = new RedisDaoImpl();
//
//        Redis redis = new Redis();
//        redis.setId(1l);
//        redis.setWeight(11);
//        assertTrue(redisAbstractDao.getEntityStorage().getEntities().size() == 0);
//        assertSame(redisAbstractDao.save(redis), redis);
//
//        assertTrue(redisAbstractDao.getEntityStorage().getEntities().size() == 1);
//
//        Redis redisNull = new Redis();
//        redisNull.setId(null);
//        redisNull.setWeight(12);
//        redisAbstractDao.save(redisNull);
//
//        assertTrue(redisAbstractDao.getEntityStorage().getEntities().size() == 2);
//        assertTrue(redisNull.getId() == 2l);
//
//        Redis redis1 = new Redis();
//        redis1.setId(12l);
//        redis1.setWeight(6);
//        redisAbstractDao.save(redis1);
//
//        assertTrue(redisAbstractDao.getEntityStorage().getEntities().size() == 3);
//        assertTrue(redis1.getId() == 12l);
//
//    }
//
//    @Test
//    public void saveTestException() throws UserAlreadyExist {
//        AbstractDao<Redis> redisAbstractDao = new RedisDaoImpl();
//        Redis redis = new Redis();
//        redis.setId(1l);
//        redis.setWeight(11);
//
//        redisAbstractDao.save(redis);
//
//        try {
//            redisAbstractDao.save(redis);
//        } catch (UserAlreadyExist e) {
//            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist: UserAlreadyExist");
//        }
//
//
//    }
//
//    @Test
//    public void getTest() throws UserAlreadyExist {
//        AbstractDao<Redis> redisAbstractDao = new RedisDaoImpl();
//        Redis redis = new Redis();
//        redis.setId(1l);
//        redis.setWeight(11);
//
//        redisAbstractDao.save(redis);
//        assertSame(redisAbstractDao.get(redis.getId()), redis);
//
//        Redis redis1 = new Redis();
//        redis1.setId(null);
//        redis1.setWeight(11);
//
//        redisAbstractDao.save(redis1);
//        assertSame(redisAbstractDao.get(redis1.getId()), redis1);
//        assertTrue(redisAbstractDao.get(redis1.getId()).getId() == 2l);
//
//
//    }
//
//    @Test
//    public void deleteTest() throws UserAlreadyExist {
//        AbstractDao<Redis> redisAbstractDao = new RedisDaoImpl();
//        Redis redis = new Redis();
//        redis.setId(1l);
//        redis.setWeight(11);
//
//        redisAbstractDao.save(redis);
//        assertTrue(redisAbstractDao.getEntityStorage().getEntities().size() == 1);
//        assertTrue(redisAbstractDao.delete(redis.getId()) == true);
//        assertTrue(redisAbstractDao.getEntityStorage().getEntities().size() == 0);
//
//    }
//
//    @Test
//    public void updateTest() throws UserAlreadyExist, UserNotFound {
//        AbstractDao<Redis> redisAbstractDao = new RedisDaoImpl();
//        Redis redis = new Redis();
//        redis.setId(1l);
//        redis.setWeight(11);
//
//        Redis redis1 = new Redis();
//        redis1.setId(1l);
//        redis1.setWeight(15);
//
//        Redis redis2 = new Redis();
//        redis2.setId(null);
//        redis2.setWeight(16);
//
//        Redis redis3 = new Redis();
//        redis3.setId(20l);
//        redis3.setWeight(18);
//
//        redisAbstractDao.save(redis);
//
//        assertTrue(redisAbstractDao.getEntityStorage().getEntities().get(redis1.getId()).getWeight() == 11);
//
//        redisAbstractDao.update(redis1);
//
//        assertTrue(redisAbstractDao.getEntityStorage().getEntities().size() == 1);
//
//        assertTrue(redisAbstractDao.getEntityStorage().getEntities().get(redis1.getId()).getWeight() == 15);
//
//        try {
//            redisAbstractDao.update(redis2);
//        } catch (UserNotFound e) {
//            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound: UserNotFound");
//        }
//
//        try {
//            redisAbstractDao.update(redis3);
//        } catch (UserNotFound e) {
//            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound: UserNotFound");
//        }
//
//
//    }
//
//}
