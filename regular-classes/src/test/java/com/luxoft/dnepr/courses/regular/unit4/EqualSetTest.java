package com.luxoft.dnepr.courses.regular.unit4;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class EqualSetTest {

    @Test
    public void addNullTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add(null);
        assertFalse(equalSet.add(null));
    }

    @Test
    public void addTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add("a");
        assertFalse(equalSet.add("a"));
        equalSet.add("b");
        assertTrue(equalSet.size() == 2);
    }

    @Test
    public void addAllTest() {
        Set<Integer> equalSet = new EqualSet<>();

        List<Integer> list = new ArrayList<>();
        list.add(7);
        list.add(7);
        list.add(5);
        assertTrue(equalSet.addAll(list));
        assertTrue(equalSet.size() == 2);
        assertFalse(equalSet.addAll(list));


    }

    @Test
    public void addAllNullTest() {

        try {
            Set<Integer> equalSet = new EqualSet<>();
            equalSet.addAll(null);
        } catch (NullPointerException e) {
            assertEquals("" + e, "java.lang.NullPointerException");
        }
    }

    @Test
    public void clearTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(5);
        equalSet.clear();
        assertTrue(equalSet.isEmpty());
    }

    @Test
    public void containsTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        assertTrue(equalSet.contains(6));
        assertFalse(equalSet.contains(7));
        List<Integer> list = new ArrayList<>();
        list.add(7);
        list.add(7);
        list.add(5);
        assertFalse(equalSet.containsAll(list));
        List<Integer> list1 = new ArrayList<>();
        list1.add(6);
        assertTrue(equalSet.containsAll(list1));

    }

    @Test
    public void isEmptyTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        assertFalse(equalSet.isEmpty());
        equalSet.clear();
        assertTrue(equalSet.isEmpty());
    }

    @Test
    public void removeTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        assertTrue(equalSet.remove(6));
        assertTrue(equalSet.size() == 1);
        assertFalse(equalSet.remove(7));
        equalSet.add(null);
        assertTrue(equalSet.remove(null));
        assertTrue(equalSet.size() == 1);
    }

    @Test
    public void removeAllTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        List<Integer> list = new ArrayList<>();
        list.add(7);
        list.add(8);
        list.add(5);

        assertTrue(equalSet.removeAll(list));
        assertTrue(equalSet.size() == 1);
        assertFalse(equalSet.removeAll(list));
        assertTrue(equalSet.size() == 1);

    }

    @Test
    public void retainAllTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        List<Integer> list = new ArrayList<>();
        list.add(7);
        list.add(8);
        list.add(5);

        assertTrue(equalSet.retainAll(list));
        assertTrue(equalSet.size() == 1);

        equalSet.add(7);
        equalSet.add(5);
        assertTrue(equalSet.size() == 3);
        assertFalse(equalSet.retainAll(list));


    }

    @Test
    public void sizeTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        equalSet.add(null);
        List<Integer> list = new ArrayList<>();
        list.add(7);
        list.add(8);
        list.add(5);
        equalSet.addAll(list);
        assertTrue(equalSet.size() == 5);
    }

    @Test
    public void toArrayTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        equalSet.add(null);

        List<Integer> list = new ArrayList<>(equalSet.size());
        for (Integer i : equalSet)
            list.add(i);
        assertEquals(equalSet.toArray(), list.toArray());


    }

    @Test
    public void toArrayParametersTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        equalSet.add(null);
        Integer[] integers = new Integer[equalSet.size()];
        integers = equalSet.toArray(integers);
        Set<Integer> set1 = new EqualSet<>();
        for (Integer i : integers)
            set1.add(i);
        assertEquals(equalSet, set1);


    }

    @Test
    public void equalsTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        equalSet.add(null);
        Set<Integer> equalSet1 = new EqualSet<>();
        equalSet1.add(6);
        equalSet1.add(null);
        equalSet1.add(8);
        assertEquals(equalSet, equalSet1);
        equalSet.add(6);
        assertEquals(equalSet, equalSet1);
        equalSet.add(7);


    }

    @Test
    public void hashCodeTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(6);
        equalSet.add(8);
        equalSet.add(null);
        Set<Integer> equalSet1 = new EqualSet<>();
        equalSet1.add(6);
        equalSet1.add(null);
        equalSet1.add(8);
        assertEquals(equalSet.hashCode(), equalSet1.hashCode());
    }

}
