package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class LuxoftUtilsTest {
    @Test
    public void testSortArray() throws Exception {
        String[] a = {"134", "dsf", "Dsf", "Das", "Das ", "235f"};
        String[] aTrue = {"134", "235f", "Das", "Das ", "Dsf", "dsf"};
        String[] aFalse = {"dsf", "Dsf", "Das ", "Das", "235f", "134"};
        Assert.assertArrayEquals(aTrue, LuxoftUtils.sortArray(a, true));
        Assert.assertArrayEquals(aFalse, LuxoftUtils.sortArray(a, false));
    }

    @Test
    public void testWordAverageLength() throws Exception {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat"), 0.000001);
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat    "), 0.000001);
    }

    @Test
    public void testReverseWords() throws Exception {
        Assert.assertEquals("cba trf rgrg ", LuxoftUtils.reverseWords("abc frt grgr "));
        Assert.assertEquals("  cba trf gfrgrg ", LuxoftUtils.reverseWords("  abc frt grgrfg "));
    }


    @Test
    public void testGetCharEntries() throws Exception {
        char[] c = {'b', 'f', 'a', 'c', 'd', 'e'};

        char[] d = {'a', 'b', 'f', 'c', 'd', 'e', 'h', 'i', 'j', 'k'};

        Assert.assertArrayEquals(c, LuxoftUtils.getCharEntries("aabbbbedcfff  "));
        Assert.assertArrayEquals(d, LuxoftUtils.getCharEntries("aaaabbbbedccfffikjh     "));

        char[] e = {'a', 'b', 'f', 'c', 'd', 'e', 'h', 'i', 'j', 'k','o'};

        Assert.assertArrayEquals(e, LuxoftUtils.getCharEntries("aaaabbbbedccfffikjho     "));

    }

    @Test
    public void testCalculateOverallArea() throws Exception {
        List<Figure> figures = new ArrayList<Figure>();
        figures.add(new Circle(1));
        figures.add(new Square(1));
        figures.add(new Hexagon(1));
        Assert.assertEquals(6.739668864943109, LuxoftUtils.calculateOverallArea(figures), 0.000001);
        Assert.assertEquals(6.739668, LuxoftUtils.calculateOverallArea(figures), 0.000001);
    }
}
