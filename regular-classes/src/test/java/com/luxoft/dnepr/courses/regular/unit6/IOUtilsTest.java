package com.luxoft.dnepr.courses.regular.unit6;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class IOUtilsTest {

    @Test
    public void saveToOsAndLoadFromIsTest() throws IOException {

        PipedOutputStream pos = new PipedOutputStream();
        PipedInputStream pis = new PipedInputStream(pos);

        PersonImpl person = new PersonImpl();
        person.setAge(20);
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        //person.setName("Galja");

        PersonImpl father = new PersonImpl();
        father.setAge(40);
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");

        PersonImpl mother = new PersonImpl();
        mother.setAge(40);
        mother.setEthnicity("ukrainian");
        mother.setGender(Gender.FEMALE);
        mother.setName("Olya");

        PersonImpl grandmother = new PersonImpl();
        grandmother.setAge(70);
        grandmother.setGender(Gender.FEMALE);
        grandmother.setName("Sasha");

        father.setMother(grandmother);

        person.setFather(father);

        person.setMother(mother);

        FamilyTree obj = FamilyTreeImpl.create(person);

        IOUtils.save(pos, obj);

        FamilyTree ftLoad = IOUtils.load(pis);

        assertSame(null, ftLoad.getRoot().getName());
        assertEquals(Gender.FEMALE, ftLoad.getRoot().getGender());
        assertEquals(20, ftLoad.getRoot().getAge());
        assertEquals("ukrainian", ftLoad.getRoot().getEthnicity());

        assertEquals("Vasiliy", ftLoad.getRoot().getFather().getName());
        assertEquals(Gender.MALE, ftLoad.getRoot().getFather().getGender());
        assertEquals(40, ftLoad.getRoot().getFather().getAge());
        assertEquals("ukrainian", ftLoad.getRoot().getFather().getEthnicity());

        assertEquals("Olya", ftLoad.getRoot().getMother().getName());
        assertEquals(Gender.FEMALE, ftLoad.getRoot().getMother().getGender());
        assertEquals(40, ftLoad.getRoot().getMother().getAge());
        assertEquals("ukrainian", ftLoad.getRoot().getMother().getEthnicity());

        assertEquals("Sasha", ftLoad.getRoot().getFather().getMother().getName());
        assertEquals(Gender.FEMALE, ftLoad.getRoot().getFather().getMother().getGender());
        assertEquals(70, ftLoad.getRoot().getFather().getMother().getAge());
        assertSame(null, ftLoad.getRoot().getFather().getMother().getEthnicity());

    }

//    @Test
//    public void saveToFileAndLoadFromFileTest() throws IOException {
//
//        PersonImpl person = new PersonImpl();
//        person.setAge(20);
//        person.setEthnicity("ukrainian");
//        person.setGender(Gender.FEMALE);
//        person.setName("Galja");
//
//        PersonImpl father = new PersonImpl();
//        father.setAge(40);
//        father.setEthnicity("ukrainian");
//        father.setGender(Gender.MALE);
//        father.setName("Vasiliy");
//
//        PersonImpl mother = new PersonImpl();
//        mother.setAge(40);
//        mother.setEthnicity("ukrainian");
//        mother.setGender(Gender.FEMALE);
//        mother.setName("Olya");
//
//        PersonImpl grandmother = new PersonImpl();
//        grandmother.setAge(70);
//        grandmother.setGender(Gender.FEMALE);
//        grandmother.setName("Sasha");
//
//        father.setMother(grandmother);
//
//        person.setFather(father);
//
//        person.setMother(mother);
//
//        FamilyTree obj = FamilyTreeImpl.create(person);
//
//        IOUtils.save("C:\\1.txt", obj);
//
//        FamilyTree ftLoad = IOUtils.load("C:\\1.txt");
//
//        assertEquals("Galja", ftLoad.getRoot().getName());
//        assertEquals(Gender.FEMALE, ftLoad.getRoot().getGender());
//        assertEquals(20, ftLoad.getRoot().getAge());
//        assertEquals("ukrainian", ftLoad.getRoot().getEthnicity());
//
//        assertEquals("Vasiliy", ftLoad.getRoot().getFather().getName());
//        assertEquals(Gender.MALE, ftLoad.getRoot().getFather().getGender());
//        assertEquals(40, ftLoad.getRoot().getFather().getAge());
//        assertEquals("ukrainian", ftLoad.getRoot().getFather().getEthnicity());
//
//        assertEquals("Olya", ftLoad.getRoot().getMother().getName());
//        assertEquals(Gender.FEMALE, ftLoad.getRoot().getMother().getGender());
//        assertEquals(40, ftLoad.getRoot().getMother().getAge());
//        assertEquals("ukrainian", ftLoad.getRoot().getMother().getEthnicity());
//
//        assertEquals("Sasha", ftLoad.getRoot().getFather().getMother().getName());
//        assertEquals(Gender.FEMALE, ftLoad.getRoot().getFather().getMother().getGender());
//        assertEquals(70, ftLoad.getRoot().getFather().getMother().getAge());
//        assertSame(null, ftLoad.getRoot().getFather().getMother().getEthnicity());
//    }
}
