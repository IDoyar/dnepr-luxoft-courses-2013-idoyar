package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

public class LuxoftUtilsTest {

    @Test
    public void getMonthNameTest() {
        Assert.assertEquals("January", LuxoftUtils.getMonthName(1, "en"));
        Assert.assertEquals("Декабрь", LuxoftUtils.getMonthName(12, "ru"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(5, "hindi"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(13, "hindi"));
        Assert.assertEquals("Неизвестный месяц", LuxoftUtils.getMonthName(-5, "ru"));
    }

    @Test
    public void binaryToDecimalTest() {
        Assert.assertEquals("2", LuxoftUtils.binaryToDecimal("10"));
        Assert.assertEquals("4", LuxoftUtils.binaryToDecimal("100"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("102"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("1s"));
    }

    @Test
    public void decimalToBinaryTest() {
        Assert.assertEquals("10", LuxoftUtils.decimalToBinary("2"));
        Assert.assertEquals("100", LuxoftUtils.decimalToBinary("4"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("1s"));
    }

    @Test
    public void sortArrayTest() {
        int[] a = {1, -6, -4, 3};
        int[] b = {-6, -4, 1, 3};
        int[] c = {3, 1, -4, -6};
        Assert.assertArrayEquals(b, LuxoftUtils.sortArray(a, true));
        Assert.assertArrayEquals(c, LuxoftUtils.sortArray(a, false));
    }
}
