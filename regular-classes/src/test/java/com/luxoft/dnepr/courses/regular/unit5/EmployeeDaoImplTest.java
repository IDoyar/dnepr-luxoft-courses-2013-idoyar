package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.AbstractDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.EntityDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import org.junit.Test;

import static org.junit.Assert.*;

//import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;

public class EmployeeDaoImplTest {

    @Test
    public void saveTest() throws UserAlreadyExist {

       // AbstractDao<Employee> employeeAbstractDao = new EmployeeDaoImpl();
        AbstractDao<Entity> employeeAbstractDao = new EntityDaoImpl();

        Employee employee = new Employee();
        employee.setId(1l);
        employee.setSalary(11);

        assertTrue(employeeAbstractDao.getEntityStorage().getEntities().size() == 0);

        assertSame(employeeAbstractDao.save(employee), employee);

        assertTrue(employeeAbstractDao.getEntityStorage().getEntities().size() == 1);

        assertTrue(employee.getId() == 1l);

        Employee employeeNull = new Employee();

        employeeNull.setId(null);

        employeeNull.setSalary(12);

        employeeAbstractDao.save(employeeNull);

        assertTrue(employeeAbstractDao.getEntityStorage().getEntities().size() == 2);
        assertTrue(employeeNull.getId() == 2l);

        Employee employee1 = new Employee();
        employee1.setId(12l);
        employee1.setSalary(6);
        employeeAbstractDao.save(employee1);

        assertTrue(employeeAbstractDao.getEntityStorage().getEntities().size() == 3);
        assertTrue(employee1.getId() == 12l);

    }

    @Test
    public void saveTestException() throws UserAlreadyExist {

       // AbstractDao<Employee> employeeAbstractDao = new EmployeeDaoImpl();
              AbstractDao<Entity> employeeAbstractDao = new EntityDaoImpl();

        Employee employee = new Employee();

        employee.setId(1l);

        employee.setSalary(11);

        employeeAbstractDao.save(employee);

        try {
            employeeAbstractDao.save(employee);
        } catch (UserAlreadyExist e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist: UserAlreadyExist");
        }

    }

    @Test
    public void getTest() throws UserAlreadyExist {

        //AbstractDao<Employee> employeeAbstractDao = new EmployeeDaoImpl();
        AbstractDao<Entity> employeeAbstractDao = new EntityDaoImpl();

        Employee employee = new Employee();
        employee.setId(1l);
        employee.setSalary(11);

        employeeAbstractDao.save(employee);
        assertSame(employeeAbstractDao.get(employee.getId()), employee);

        Employee employee1 = new Employee();
        employee1.setId(null);
        employee1.setSalary(11);

        employeeAbstractDao.save(employee1);
        assertSame(employeeAbstractDao.get(employee1.getId()), employee1);
        assertTrue(employeeAbstractDao.get(employee1.getId()).getId() == 2l);


    }

    @Test
    public void deleteTest() throws UserAlreadyExist {

        //AbstractDao<Employee> employeeAbstractDao = new EmployeeDaoImpl();
        AbstractDao<Entity> employeeAbstractDao = new EntityDaoImpl();

        Employee employee = new Employee();
        employee.setId(1l);
        employee.setSalary(11);

        employeeAbstractDao.save(employee);
        assertTrue(employeeAbstractDao.getEntityStorage().getEntities().size() == 1);
        assertTrue(employeeAbstractDao.delete(employee.getId()));
        assertTrue(employeeAbstractDao.getEntityStorage().getEntities().size() == 0);

    }

    @Test
    public void updateTest() throws UserAlreadyExist, UserNotFound {

        //AbstractDao<Employee> employeeAbstractDao = new EmployeeDaoImpl();
        AbstractDao<Entity> employeeAbstractDao = new EntityDaoImpl();

        Employee employee = new Employee();
        employee.setId(1l);
        employee.setSalary(11);

        Employee employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(15);

        Employee employee2 = new Employee();
        employee2.setId(null);
        employee2.setSalary(16);

        Employee employee3 = new Employee();
        employee3.setId(20l);
        employee3.setSalary(18);

        employeeAbstractDao.save(employee);

        Employee saveEmployee = (Employee)(employeeAbstractDao.getEntityStorage().getEntities().get(employee1.getId()));

        assertTrue(saveEmployee.getSalary() == 11);

        employeeAbstractDao.update(employee1);

        assertTrue(employeeAbstractDao.getEntityStorage().getEntities().size() == 1);

        Employee saveEmployee1 = (Employee)(employeeAbstractDao.getEntityStorage().getEntities().get(employee1.getId()));

        assertTrue(saveEmployee1.getSalary() == 15);
        //assertTrue(employeeAbstractDao.getEntityStorage().getEntities().get(employee1.getId()).getSalary() == 15);

        try {
            employeeAbstractDao.update(employee2);
        } catch (UserNotFound e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound: UserNotFound");
        }

        try {
            employeeAbstractDao.update(employee3);
        } catch (UserNotFound e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound: UserNotFound");
        }

    }

}
