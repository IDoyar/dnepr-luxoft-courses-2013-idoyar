package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class WalletTest {

    @Test
    public void testCheckWithdrawal() throws InsufficientWalletAmountException, WalletIsBlockedException {
        WalletInterface walletInterface = new Wallet();

        walletInterface.setId(123l);
        walletInterface.setStatus(WalletStatus.BLOCKED);
        walletInterface.setAmount(BigDecimal.valueOf(100));
        walletInterface.setMaxAmount(BigDecimal.valueOf(500));

        BigDecimal amountToWithdraw = new BigDecimal(200);

        try {
            walletInterface.checkWithdrawal(amountToWithdraw);
        } catch (WalletIsBlockedException e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException: User wallet is blocked");

        }

        walletInterface.setStatus(WalletStatus.ACTIVE);

        try {
            walletInterface.checkWithdrawal(amountToWithdraw);
        } catch (InsufficientWalletAmountException e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException: User has insufficient funds");

        }


    }

    @Test
    public void testWithdraw() {
        WalletInterface walletInterface1 = new Wallet();

        walletInterface1.setAmount(BigDecimal.valueOf(100));
        BigDecimal amountToWithdraw1 = new BigDecimal(50);
        walletInterface1.withdraw(amountToWithdraw1);
        assertEquals(BigDecimal.valueOf(50), walletInterface1.getAmount());


    }

    @Test
    public void checkTransfer() throws LimitExceededException, WalletIsBlockedException {
        WalletInterface walletInterface2 = new Wallet();

        walletInterface2.setId(123l);
        walletInterface2.setStatus(WalletStatus.BLOCKED);
        walletInterface2.setAmount(BigDecimal.valueOf(100));
        walletInterface2.setMaxAmount(BigDecimal.valueOf(500));

        BigDecimal amountToWithdraw2 = new BigDecimal(600);

        try {
            walletInterface2.checkTransfer(amountToWithdraw2);
        } catch (WalletIsBlockedException e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException: User wallet is blocked");

        }

        walletInterface2.setStatus(WalletStatus.ACTIVE);

        try {
            walletInterface2.checkTransfer(amountToWithdraw2);
        } catch (LimitExceededException e) {
            assertEquals("" + e, "com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException: User wallet limit exceeded");

        }

    }

    @Test
    public void testTransfer() {
        WalletInterface walletInterface1 = new Wallet();

        walletInterface1.setAmount(BigDecimal.valueOf(100));
        BigDecimal amountToWithdraw1 = new BigDecimal(50);
        walletInterface1.transfer(amountToWithdraw1);
        assertEquals(BigDecimal.valueOf(150), walletInterface1.getAmount());


    }
}
