//package com.luxoft.dnepr.courses.regular.unit5.dao;
//
//
//import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
//import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
//
//
//public class EmployeeDaoImpl extends AbstractDao<Employee> {
//
//    @Override
//    public Employee update(Employee entity) throws UserNotFound {
//
//        if (entity.getId() == null || getEntityStorage().getEntities().get(entity.getId()) == null)
//            throw new UserNotFound("UserNotFound");
//
//        Employee entityInDataBase = getEntityStorage().getEntities().get(entity.getId());
//        entityInDataBase.setSalary(entity.getSalary());
//
//        return entityInDataBase;
//
//    }
//
//
//}
