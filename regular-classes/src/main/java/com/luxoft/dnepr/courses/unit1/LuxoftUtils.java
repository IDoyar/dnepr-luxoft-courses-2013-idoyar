package com.luxoft.dnepr.courses.unit1;

public final class LuxoftUtils {

    private LuxoftUtils() {
    }

    public static String getMonthName(int monthOrder, String language) {
        if (!(language.equals("en")) && !(language.equals("ru")))
            return "Unknown Language";
        if (language.equals("en"))
            switch (monthOrder) {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return "Unknown Month";
            }
        else
            switch (monthOrder) {
                case 1:
                    return "Январь";
                case 2:
                    return "Февраль";
                case 3:
                    return "Март";
                case 4:
                    return "Апрель";
                case 5:
                    return "Май";
                case 6:
                    return "Июнь";
                case 7:
                    return "Июль";
                case 8:
                    return "Август";
                case 9:
                    return "Сентябрь";
                case 10:
                    return "Октябрь";
                case 11:
                    return "Ноябрь";
                case 12:
                    return "Декабрь";
                default:
                    return "Неизвестный месяц";
            }
    }

    public static String binaryToDecimal(String binaryNumber) {
        try {
            int binaryNumberToInteger = Integer.parseInt(binaryNumber);
            int theNumberOfDigitsOfTheBinaryNumberToInteger = (int) Math.floor(Math.log10(binaryNumberToInteger) + 1);
            int decimalNumber = 0;
            for (int i = 0; i < theNumberOfDigitsOfTheBinaryNumberToInteger; i++) {
                if (Integer.parseInt(String.valueOf(binaryNumber.charAt(i))) != 1 && (Integer.parseInt(String.valueOf(binaryNumber.charAt(i))) != 0))
                    return "Not binary";
                else
                    decimalNumber += Math.pow(2, theNumberOfDigitsOfTheBinaryNumberToInteger - i - 1) * Integer.parseInt(String.valueOf(binaryNumber.charAt(i)));
            }
            String decimalNumberToString = String.valueOf(decimalNumber);
            return decimalNumberToString;
        } catch (NumberFormatException e) {
            return "Not binary";
        }
    }

    public static String decimalToBinary(String decimalNumber) {
        try {
            int decimalNumberToInteger = Integer.parseInt(decimalNumber);
            StringBuilder reverseBinaryNumber = new StringBuilder();
            int remainderInDivisionDecimalNumberToIntegerOnTwo;
            while (decimalNumberToInteger != 0) {
                remainderInDivisionDecimalNumberToIntegerOnTwo = decimalNumberToInteger;
                remainderInDivisionDecimalNumberToIntegerOnTwo %= 2;
                reverseBinaryNumber.append(remainderInDivisionDecimalNumberToIntegerOnTwo);
                decimalNumberToInteger /= 2;
            }
            String binaryNumberToString = reverseBinaryNumber.reverse().toString();
            return binaryNumberToString;
        } catch (NumberFormatException e) {
            return "Not decimal";
        }
    }

    public static int[] sortArray(int[] array, boolean asc) {
        int lengthOfArray = array.length;
        int[] auxiliaryArray = new int[lengthOfArray];
        for (int k = 0; k < lengthOfArray; k++)
            auxiliaryArray[k] = array[k];
        if (asc) {
            for (int i = 0; i < lengthOfArray; i++)
                for (int j = 0; j < lengthOfArray - i - 1; j++)
                    if (auxiliaryArray[j] > auxiliaryArray[j + 1]) {
                        auxiliaryArray[j] = auxiliaryArray[j] + auxiliaryArray[j + 1];
                        auxiliaryArray[j + 1] = auxiliaryArray[j] - auxiliaryArray[j + 1];
                        auxiliaryArray[j] = auxiliaryArray[j] - auxiliaryArray[j + 1];
                    }
        } else {
            for (int i = 0; i < lengthOfArray; i++)
                for (int j = 0; j < lengthOfArray - i - 1; j++)
                    if (auxiliaryArray[j] < auxiliaryArray[j + 1]) {
                        auxiliaryArray[j] = auxiliaryArray[j] + auxiliaryArray[j + 1];
                        auxiliaryArray[j + 1] = auxiliaryArray[j] - auxiliaryArray[j + 1];
                        auxiliaryArray[j] = auxiliaryArray[j] - auxiliaryArray[j + 1];
                    }
        }
        return auxiliaryArray;
    }
}
