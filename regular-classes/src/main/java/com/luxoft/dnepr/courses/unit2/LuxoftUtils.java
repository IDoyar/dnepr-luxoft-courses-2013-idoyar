package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

public final class LuxoftUtils {

    private LuxoftUtils() {
    }

    /**
     * @param array String array
     * @param asc
     * @return sorted array in ascending order, if asc=<code>true</code>, otherwise in descending order.
     */
    public static String[] sortArray(String[] array, boolean asc) {

        String[] arrayCopy = array.clone();
        mergeSort(arrayCopy, asc);
        return arrayCopy;
    }

    private static void mergeSort(String[] array, boolean asc) {

        mergeSort(array, 0, array.length - 1, asc);
    }

    private static void mergeSort(String[] array, int left, int right, boolean asc) {

        if (left >= right) return;
        int m = (left + right) / 2;
        mergeSort(array, left, m, asc);
        mergeSort(array, m + 1, right, asc);
        merge(array, left, m, right, asc);
    }

    private static void merge(String[] array, int left, int middle, int right, boolean asc) {

        if (middle + 1 > right) return;
        String[] copyArray = copyToArray(array, left, middle, right);

        int k = left;
        int j = right;

        for (int i = left; i != right + 1; i++) {
            if (needToMergeLeft(copyArray, k, j, asc))
                array[i] = copyArray[k++];
            else
                array[i] = copyArray[j--];
        }

    }

    private static String[] copyToArray(String[] array, int left, int middle, int right) {

        String[] copyArray = new String[array.length];
        for (int i = left; i != middle + 1; i++) {
            copyArray[i] = array[i];
        }
        for (int i = middle + 1; i != right + 1; i++) {
            copyArray[i] = array[right + middle + 1 - i];
        }
        return copyArray;
    }

    private static boolean needToMergeLeft(String[] copyArray, int k, int j, boolean asc) {

        return (copyArray[k].compareTo(copyArray[j]) <= 0) == asc;
    }

    /**
     * Calculate the average length of word in str
     *
     * @param str String
     * @return wordAverageLengthInStr double
     */
    public static double wordAverageLength(String str) {
        String[] theArrayOfWords = str.split(" ");
        int lengthOfArray = theArrayOfWords.length;
        double wordAverageLengthInStr = 0;
        for (int i = 0; i < lengthOfArray; i++)
            wordAverageLengthInStr += theArrayOfWords[i].length();
        return wordAverageLengthInStr / lengthOfArray;
    }

    /**
     * Reverse all words in str
     *
     * @param str String
     * @return the newStr which contain all words in reverse order
     */
    public static String reverseWords(String str) {
        StringTokenizer splitTheStr = new StringTokenizer(str, " ", true);
        StringBuilder buildTheNewStr = new StringBuilder();
        while (splitTheStr.hasMoreTokens())
            buildTheNewStr.append(new StringBuilder(splitTheStr.nextToken()).reverse());
        return buildTheNewStr.toString();
    }

    /**
     * To get char array which contains the chars in order by their frequency
     *
     * @param str String
     * @return array char array
     */
    public static char[] getCharEntries(String str) {

        char[] arrayOfChars = new char[createSortedMap(createMap(str)).size()];
        int i = 0;
        for (Map.Entry<Character, Integer> entry : createSortedMap(createMap(str)).entrySet()) {
            arrayOfChars[i] = entry.getKey();
            i++;
        }
        return arrayOfChars;
    }

    private static Map<Character, Integer> createMap(String str) {
        final Map<Character, Integer> map = new HashMap<Character, Integer>();
        int lengthOfStr = str.length();
        StringTokenizer splitTheStr = new StringTokenizer(str, " ", false);
        while (splitTheStr.hasMoreTokens()) {
            String currentWord = splitTheStr.nextToken();
            for (int j = 0; j < currentWord.length(); j++)
                map.put(currentWord.charAt(j), 0);
        }
        for (int i = 0; i < lengthOfStr; i++)
            if (map.containsKey(str.charAt(i))) {
                map.put(str.charAt(i), map.get(str.charAt(i)) + 1);
            }
        return map;
    }

    private static Map<Character, Integer> createSortedMap(final Map<Character, Integer> map) {
        final Map<Character, Integer> sortedMap = new TreeMap<Character, Integer>(new Comparator<Character>() {
            @Override
            public int compare(Character leftKey, Character rightKey) {
                int result = map.get(rightKey).compareTo(map.get(leftKey));
                return result == 0 ? leftKey.compareTo(rightKey) : result;
            }
        });
        sortedMap.putAll(map);
        return sortedMap;
    }

    /**
     * To calculate sum of area of the figures
     *
     * @param figures List<Figure>
     * @return sumOfArea double
     */
    public static double calculateOverallArea(List<Figure> figures) {
        double sumOfArea = 0;
        for (int i = 0; i < figures.size(); i++)
            sumOfArea += figures.get(i).calculateArea();
        return sumOfArea;
    }
}
