package com.luxoft.dnepr.courses.regular.unit7;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

/**
 * Crawls files and directories, searches for text files and counts word
 * occurrences.
 */
public class FileCrawler {

    private WordStorage wordStorage = new WordStorage();
    private List<File> allFiles = new ArrayList<File>();
    private String rootFolder;
    private ExecutorService executor;

    /**
     * Getters and Setters
     */
    public WordStorage getWordStorage() {
        return wordStorage;
    }

    public void setWordStorage(WordStorage wordStorage) {
        this.wordStorage = wordStorage;
    }

    public List<File> getAllFiles() {
        return allFiles;
    }

    public void setAllFiles(List<File> allFiles) {
        this.allFiles = allFiles;
    }

    public String getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }

    /**
     * Constructor
     */
    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.executor = Executors.newFixedThreadPool(maxNumberOfThreads);

    }

    /**
     * Performs crawling using multiple threads. This method should wait until
     * all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() throws InterruptedException, ExecutionException {

        File rootFile = new File(rootFolder);

        Collection<Future> futures = new ArrayList<Future>();

        listFilesForFolder(rootFile);

        for (final File file : allFiles) {
            futures.add(executor.submit(new FileProcessor(file, wordStorage)));
        }
        for (final Future future : futures) {
            future.get();
        }

        executor.shutdown();


        FileCrawlerResults fileCrawlerResults = new FileCrawlerResults(allFiles, wordStorage.getWordStatistics());

        return fileCrawlerResults;
    }

    private static class FileProcessor implements Runnable {

        private static final String REPLACE_PATTERN = "(?U)[\\W_]+";
        private File file;
        private WordStorage storage;

        public FileProcessor(File file, WordStorage storage) {
            this.file = file;
            this.storage = storage;
        }

        @Override
        public void run() {

            Pattern p = Pattern.compile(REPLACE_PATTERN, Pattern.UNICODE_CHARACTER_CLASS);

            String parts[] = p.split(readTextFromFile(file));

            synchronized (storage) {
                for (String s : parts) {
                    storage.save(s);
                }
            }
        }
    }

    /**
     * Read text from file and put it to StringBuilder
     *
     * @return StringBuilder
     */
    private static String readTextFromFile(File file) {

        StringBuilder sb = new StringBuilder();

        try {

            InputStreamReader isr = new InputStreamReader(new FileInputStream(file.getAbsoluteFile()), "UTF-8");
            BufferedReader in = new BufferedReader(isr);
            try {

                String s;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");

                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
        }

        return sb.toString();
    }

    private void listFilesForFolder(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                if (fileEntry.getPath().endsWith(".txt")) {
                    allFiles.add(fileEntry);
                }
            }
        }
    }

    public static void main(String args[]) throws InterruptedException, ExecutionException {

        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();

    }
}


