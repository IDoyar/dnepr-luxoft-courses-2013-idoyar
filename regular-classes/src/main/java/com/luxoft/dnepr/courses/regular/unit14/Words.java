package com.luxoft.dnepr.courses.regular.unit14;

public class Words {

	private int knownWordsCount = 0;
	private int unknownWordsCount = 0;
	
	public void result(int sizeOfVocabulary) {
		int approximatelyKnownWordsCount = sizeOfVocabulary * (knownWordsCount + 1) / (knownWordsCount + unknownWordsCount + 1);
		System.out.println("You know approximately "+approximatelyKnownWordsCount+" words from this vocabulary");
	}

    public void incrementKnownWordsCount() {
        knownWordsCount++;
    }

    public void incrementUnknownWordsCount() {
        unknownWordsCount++;
    }

}