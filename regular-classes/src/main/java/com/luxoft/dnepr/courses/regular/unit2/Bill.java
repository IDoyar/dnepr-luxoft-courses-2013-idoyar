package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    private Map<Product, CompositeProduct> product2SimilarProducts = new HashMap<Product, CompositeProduct>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {

        CompositeProduct compositeProduct = product2SimilarProducts.get(product);
        if (compositeProduct == null)// no such product previously added
            product2SimilarProducts.put(product, new CompositeProduct(product));
        else
            compositeProduct.add(product);

    }

    public static class ComparatorByPrice implements Comparator<Product> {
        public int compare(Product obj1, Product obj2) {

            double price1 = obj1.getPrice();
            double price2 = obj2.getPrice();

            return Double.compare(price2, price1);
        }
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double totalPrice = 0;
        for (CompositeProduct i : product2SimilarProducts.values())
            totalPrice += i.getPrice();
        return totalPrice;

    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {

        List<Product> products = new ArrayList<Product>(product2SimilarProducts.values());
        Collections.sort(products, new ComparatorByPrice());
        return products;

    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

}
