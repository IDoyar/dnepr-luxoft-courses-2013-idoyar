package com.luxoft.dnepr.courses.regular.unit14;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Vocabulary { 

	private List<String> vocabulary = new ArrayList<String>();

	private static class InnerVocabulary{
		static final Vocabulary ONE_INSTANCE = new Vocabulary(); 
	}

	private Vocabulary(){
   		addUniqueLongWordsToVocabulary(readSonnetsFromFile());
   	}

	public static Vocabulary getInstance(){
		return InnerVocabulary.ONE_INSTANCE;
	}

	private List<String> readSonnetsFromFile(){
		List<String> listOfWords = new ArrayList<String>();
		String currentLine;
		String[] wordsInCurrentLine = null;

        try(BufferedReader  br = new BufferedReader(new InputStreamReader(getClass().
                getResourceAsStream("/com/luxoft/dnepr/courses/regular/unit14/sonnets.txt")))) {
			while ((currentLine = br.readLine()) != null) {
				if(!currentLine.isEmpty()){
                    wordsInCurrentLine = splitLineIntoWords(currentLine);
                    listOfWords.addAll(Arrays.asList(wordsInCurrentLine));
				}
			}
   		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return listOfWords;
	}

    private String[] splitLineIntoWords(String currentLine) {
        final String REG_EXP = "[\\p{Punct}\\s]";
        String[] wordsInCurrentLine;
        wordsInCurrentLine = currentLine.split(REG_EXP);
        return wordsInCurrentLine;
    }


    private void addUniqueLongWordsToVocabulary(List<String> listOfWords){

		Set<String> temp = new HashSet<String>();

		for (String word : listOfWords) {
			if(word.length() > 3)
				temp.add(word);
		}

		vocabulary.addAll(temp);
	}

	public String getRandomWord() {
		int randomIndex = (int)(Math.random() * vocabulary.size());
		return  vocabulary.get(randomIndex);
	}

	public int getSize(){
		return vocabulary.size();
	}



}












