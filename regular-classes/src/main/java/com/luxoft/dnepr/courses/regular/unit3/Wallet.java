package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {

    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public WalletStatus getStatus() {
        return status;
    }

    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (status.equals(WalletStatus.BLOCKED))
            throw new WalletIsBlockedException(id, "User wallet is blocked");

        if (amount.compareTo(amountToWithdraw) == -1)
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, "User has insufficient funds");
    }

    public void withdraw(BigDecimal amountToWithdraw) {
        amount = amount.subtract(amountToWithdraw);
    }

    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (status.equals(WalletStatus.BLOCKED))
            throw new WalletIsBlockedException(id, "User wallet is blocked");

        if (amount.add(amountToTransfer).compareTo(maxAmount) == 1)
            throw new LimitExceededException(id, amountToTransfer, amount, "User wallet limit exceeded");

    }

    public void transfer(BigDecimal amountToTransfer) {
        amount = amount.add(amountToTransfer);
    }

}
