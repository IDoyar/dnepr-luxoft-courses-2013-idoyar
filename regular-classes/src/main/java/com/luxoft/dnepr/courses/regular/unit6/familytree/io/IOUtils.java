package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IOUtils {

    public static final String nameKey = "name";
    public static final String ethnicityKey = "ethnicity";
    public static final String genderKey = "gender";
    public static final String ageKey = "age";
    public static final String motherKey = "mother";
    public static final String fatherKey = "father";
    public static final String rootKey = "root";


    private IOUtils() {
    }

    private static FamilyTree parseFamilyTree(StringBuffer string) {
        List<Person> ancients = new ArrayList<>();
        int personCodeEnd = string.indexOf("}");
        while (personCodeEnd != string.length() - 1) {
            int personCodeStart = string.lastIndexOf("{", personCodeEnd);
            String personString = string.substring(personCodeStart + 1, personCodeEnd);
            Person person = parsePerson(personString, ancients);
            string.replace(personCodeStart, personCodeEnd + 1, "\"" + Integer.toString(ancients.size()) + "\"");
            ancients.add(person);
            personCodeEnd = string.indexOf("}");
        }

        String rootNumber = findValue(string.toString(), rootKey);
        int n = Integer.parseInt(rootNumber);
        Person root = ancients.get(n);
        return FamilyTreeImpl.create(root);
    }


    private static Person parsePerson(String string, List<Person> ancients) {
        assert string.indexOf('{') == -1;
        assert string.indexOf('}') == -1;
        String name = findValue(string, nameKey);
        String ethnicity = findValue(string, ethnicityKey);
        String gender = findValue(string, genderKey);
        String age = findValue(string, ageKey);

        Person mother = findParent(string, motherKey, ancients);
        Person father = findParent(string, fatherKey, ancients);

        PersonImpl decodedPerson = new PersonImpl();
        decodedPerson.setName(name);

        if (gender == null) {
            decodedPerson.setGender(null);
        } else {
            decodedPerson.setGender(Gender.valueOf(gender));
        }
        if (age == null) {
            decodedPerson.setAge(0);
        } else {
            decodedPerson.setAge(Integer.parseInt(age));
        }

        decodedPerson.setEthnicity(ethnicity);
        decodedPerson.setMother(mother);
        decodedPerson.setFather(father);
        return decodedPerson;
    }

    private static Person findParent(String string, String key, List<Person> ancients) {
        String parentCode = findValue(string, key);
        Person parent = null;
        if (parentCode != null) {
            int i = Integer.parseInt(parentCode);
            parent = ancients.get(i);
        }
        return parent;
    }

    private static String findValue(String string, String key) {
        int keyPosition = string.indexOf(key);
        if (keyPosition == -1) // no such key.
            return null;
        int closingBracketIndex = string.indexOf("\"", keyPosition);
        int valueStartIndex = string.indexOf("\"", closingBracketIndex + 1);
        int valueEndIndex = string.indexOf("\"", valueStartIndex + 1);
        return string.substring(valueStartIndex + 1, valueEndIndex);

    }

    private static String getJsonString(FamilyTree o) {
        return buildJson(o);
    }

    private static String buildJson(FamilyTree o) {
        Person person = o.getRoot();
        String role = "root";
        StringBuffer json = new StringBuffer();
        json.append("{");

        writePerson(person, role, json);
        json.append("}");

        return json.toString();
    }

    private static void writePerson(Person person, String role, StringBuffer json) {

        writeParameters(json, person, role);
        if (person.getFather() != null) {
            json.append(",");
            writePerson(person.getFather(), "father", json);
        }

        if (person.getMother() != null) {
            json.append(",");
            writePerson(person.getMother(), "mother", json);
        }
        json.append("}");

    }

    private static void writeParameters(StringBuffer json, Person person, String role) {

        json.append("\"").append(role).append("\"").append(":{");

        boolean flag = false;

        if (person.getName() != null) {

            json.append("\"").append("name").append("\"").append(":").append("\"").append(person.getName()).append("\"").append(",");
            flag = true;
        }
        if (person.getGender() != null) {

            json.append("\"").append("gender").append("\"").append(":").append("\"").append(person.getGender()).append("\"").append(",");
            flag = true;
        }
        if (person.getEthnicity() != null) {

            json.append("\"").append("ethnicity").append("\"").append(":").append("\"").append(person.getEthnicity()).append("\"").append(",");
            flag = true;
        }
        if (person.getAge() != 0) {

            json.append("\"").append("age").append("\"").append(":").append("\"").append(person.getAge()).append("\"").append(",");
            flag = true;
        }

        if (flag)

            json.deleteCharAt(json.length() - 1);

    }

    public static FamilyTree load(String filename) {

        try {
            return load(new FileInputStream(filename));
        } catch (FileNotFoundException e) {
            return null;
        }

    }

    public static FamilyTree load(InputStream is) {
        StringBuffer buffer = new StringBuffer();
        Scanner scanner = new Scanner(is);
        while (scanner.hasNextLine())
            buffer.append(scanner.nextLine());
        if (buffer.toString().isEmpty()) return null;
        FamilyTree familyTree = parseFamilyTree(buffer);
        return familyTree;

    }

    public static void save(String filename, FamilyTree familyTree) {

        try {
            save(new FileOutputStream(filename), familyTree);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void save(OutputStream os, FamilyTree familyTree) {

        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(os);
            outputStreamWriter.write(getJsonString(familyTree));
            outputStreamWriter.flush();
            outputStreamWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
