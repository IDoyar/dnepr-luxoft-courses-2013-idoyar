package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Collections;
import java.util.Map;

public abstract class AbstractDao<E extends Entity> implements IDao<E> {

    private EntityStorage<E> entityStorage = new EntityStorage<>();

    public EntityStorage<E> getEntityStorage() {
        return entityStorage;
    }

    public void setEntityStorage(EntityStorage<E> entityStorage) {
        this.entityStorage = entityStorage;
    }

    public Long maxIdFromEntities(Map<Long, E> entities) {
        return Collections.max(entities.keySet());
    }

    @Override
    public E save(E entity) throws UserAlreadyExist {

        if (entity.getId() == null) {
            entity.setId(maxIdFromEntities(entityStorage.getEntities()) + 1);
            entityStorage.getEntities().put(entity.getId(), entity);

        } else {
            if (entityStorage.getEntities().get(entity.getId()) == null) {
                entityStorage.getEntities().put(entity.getId(), entity);
            } else {
                throw new UserAlreadyExist("UserAlreadyExist");
            }
        }
        return entity;

    }

    @Override
    public E get(long id) {

        if (entityStorage.getEntities().get(id) == null) return null;

        return entityStorage.getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {

        if (entityStorage.getEntities().get(id) == null) return false;

        entityStorage.getEntities().remove(id);

        return true;
    }

    @Override
    public E update(E entity) throws UserNotFound {

        if (entity.getId() == null || getEntityStorage().getEntities().get(entity.getId()) == null)
            throw new UserNotFound("UserNotFound");

        E entityInDataBase = getEntityStorage().getEntities().get(entity.getId());

        entityInDataBase = entity;

        getEntityStorage().getEntities().put(entity.getId(),entityInDataBase);

        return entityInDataBase;

    }
}
