package com.luxoft.dnepr.courses.unit2.model;

public class Hexagon extends Figure {
    double side;

    public Hexagon(double side) {
        this.side = side;
    }

    public double calculateArea() {
        return 1.5 * Math.sqrt(3) * Math.pow(side, 2);
    }
}
