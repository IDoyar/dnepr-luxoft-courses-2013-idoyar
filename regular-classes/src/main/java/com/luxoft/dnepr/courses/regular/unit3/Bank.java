package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Bank implements BankInterface {
    private Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();

    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    public Bank() {

    }

    public Bank(String expectedJavaVersion) {
        String actualJavaVersion = System.getProperties().getProperty("java.version");

        if (!actualJavaVersion.equals(expectedJavaVersion))
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, "Wrong JavaVersion");

    }

    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {

        UserInterface userFrom = users.get(fromUserId);
        UserInterface userTo = users.get(toUserId);

        if (userFrom == null) throw new NoUserFoundException(fromUserId, "User " + fromUserId + " not found");

        if (userTo == null) throw new NoUserFoundException(toUserId, "User " + toUserId + " not found");

        if (userFrom.getWallet().getStatus().equals(WalletStatus.BLOCKED))
            throw new TransactionException("User '" + userFrom.getName() + "' wallet is blocked");

        if (userTo.getWallet().getStatus().equals(WalletStatus.BLOCKED))
            throw new TransactionException("User '" + userTo.getName() + "' wallet is blocked");

        if (userFrom.getWallet().getAmount().compareTo(amount) == -1)
            throw new TransactionException("User '" + userFrom.getName() + "' has insufficient funds (" + userFrom.getWallet().getAmount().setScale(2, BigDecimal.ROUND_HALF_UP) + " < " + amount.setScale(2, BigDecimal.ROUND_HALF_UP) + ")");

        if (userTo.getWallet().getAmount().add(amount).compareTo(userTo.getWallet().getMaxAmount()) == 1)
            throw new TransactionException("User '" + userTo.getName() + "' wallet limit exceeded (" + userTo.getWallet().getAmount().setScale(2, BigDecimal.ROUND_HALF_UP) + " + " + amount.setScale(2, BigDecimal.ROUND_HALF_UP) + " > " + userTo.getWallet().getMaxAmount().setScale(2, BigDecimal.ROUND_HALF_UP) + ")");

        userTo.getWallet().transfer(amount);

        userFrom.getWallet().withdraw(amount);
    }

}

