package com.luxoft.dnepr.courses.unit2.model;

public class Square extends Figure {
    double side;

    public Square(double side) {
        this.side = side;
    }

    public double calculateArea() {
        return Math.pow(side, 2);
    }
}
