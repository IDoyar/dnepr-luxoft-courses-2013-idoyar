package com.luxoft.dnepr.courses.regular.unit14;

public class Main {
	
	public static void main(String[] args) {
		
		UserInterface ui = new UserInterface();
		ui.startLinguisticAnalyzer();
	}
}
