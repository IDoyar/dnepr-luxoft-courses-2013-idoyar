package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {

    private List<Product> childProducts;

    public CompositeProduct(){
         childProducts = new ArrayList<Product>();
    }

    public CompositeProduct(Product product){
        this();
        childProducts.add(product);
    }

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        if (childProducts.isEmpty()) return null;
        return childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        if (childProducts.isEmpty()) return null;
        return childProducts.get(0).getName();

    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {

        switch (getAmount()) {
            case 0:
                return 0;
            case 1:
                return getPriceListOfProducts();
            case 2:
                return getPriceListOfProducts() * 0.95;
            default:
                return getPriceListOfProducts() * 0.9;
        }
    }

    private double getPriceListOfProducts() {
        double sum = 0;
        for (Product i : childProducts)
            sum += i.getPrice();
        return sum;

    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
