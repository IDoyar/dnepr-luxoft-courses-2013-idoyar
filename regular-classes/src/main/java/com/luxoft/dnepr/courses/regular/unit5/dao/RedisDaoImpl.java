//package com.luxoft.dnepr.courses.regular.unit5.dao;
//
//
//import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
//
//import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
//
//
//public class RedisDaoImpl extends AbstractDao<Redis> {
//
//    @Override
//    public Redis update(Redis entity) throws UserNotFound {
//
//        if (entity.getId() == null || getEntityStorage().getEntities().get(entity.getId()) == null)
//            throw new UserNotFound("UserNotFound");
//
//        Redis entityInDataBase = getEntityStorage().getEntities().get(entity.getId());
//        entityInDataBase.setWeight(entity.getWeight());
//
//        return entityInDataBase;
//
//    }
//}
