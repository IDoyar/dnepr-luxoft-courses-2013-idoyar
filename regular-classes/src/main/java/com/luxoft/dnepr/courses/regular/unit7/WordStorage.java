package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents word statistics storage.
 */
public class WordStorage {

    private Map<String, Integer> wordStatistics = new HashMap<String, Integer>();

    /**
     * Saves given word and increments count of occurrences.
     *
     * @param word
     */
    public void save(String word) {

        Integer value = wordStatistics.get(word);

        if (value != null) {
            wordStatistics.put(word, value + 1);
        } else {
            wordStatistics.put(word, 1);
        }
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {

        return Collections.unmodifiableMap(wordStatistics);
    }
}
