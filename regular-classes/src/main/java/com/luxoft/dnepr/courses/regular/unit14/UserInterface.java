package com.luxoft.dnepr.courses.regular.unit14;

import java.util.Scanner;

public class UserInterface{

    private static final String WELCOME_MESSAGE = "Welcome to my linguistic analyzer.\nI will ask you questions. " +
            "You have four options for response \n\n" +
            "1. Your answer should be \"Y\" if you know translation of the word \n" +
            "2. Your answer should be \"N\" if you don't know translation of the word \n" +
            "3. Your answer should be \"HELP\" if you want to read the rules of the analyzer once more \n" +
            "4. Your answer should be \"EXIT\" if you want to finish the analyzer and get your english knowledge\n" +
            "Analyzer will work until you response is not \"EXIT\".\n" +
            "As a result, you get your approximate English language skills in an amount words relative " +
            "to the size of the dictionary\n";

	private static final String HELP_MESSAGE = "Linguistic analyzer, author Ivan Doyar : for help type \"help\"," +
            " for exit type \"exit\", "+ "answer \"y\" or \"n\" to the questions if you want to know your level of English";

	private static final String YES = "Y";
	private static final String NO = "N";
	private static final String HELP = "HELP";
	private static final String EXIT = "EXIT";

    private Vocabulary vocabulary = Vocabulary.getInstance();

	public void startLinguisticAnalyzer() {

		String randomWord, input;
		Scanner scanner = new Scanner(System.in);
		Words userWords = new Words();
        System.out.println(WELCOME_MESSAGE);
		infiniteLoop: for (;;) {

			randomWord = vocabulary.getRandomWord();
   			System.out.println("Do you know translation of the word \""+randomWord+"\" ?");
			input = new String(scanner.nextLine()).toUpperCase();

			switch (input) {
				case HELP :{
					System.out.println(HELP_MESSAGE);
					break;
				}
	
				case EXIT :{
					userWords.result(vocabulary.getSize());
					break infiniteLoop;
				}
	
				case YES :{
					userWords.incrementKnownWordsCount();
					break;
				}
	
				case NO :{
					userWords.incrementUnknownWordsCount();
					break;
				}
	
				default: {
					System.out.println("Error while reading answer");
					break infiniteLoop;
				}
			}
		}
		scanner.close();
	}


}

