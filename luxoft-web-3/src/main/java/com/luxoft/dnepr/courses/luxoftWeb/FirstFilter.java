package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class FirstFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	  public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	            throws IOException, ServletException {
	        HttpServletRequest req = (HttpServletRequest) arg0;
	        HttpServletResponse res = (HttpServletResponse) arg1;
	        HttpSession session = req.getSession(false);
	        if (session != null) {
	            res.sendRedirect("user");
	        }
	        arg2.doFilter(req, res);
	    }

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	

}
