package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import java.util.concurrent.atomic.AtomicInteger;

public class CounterOfRequestListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestInitialized(ServletRequestEvent arg0) {

		ServletContext currentContext = arg0.getServletContext();

		countOfTotalRequests(currentContext).getAndIncrement();

	}

	private AtomicInteger countOfTotalRequests(ServletContext sc){

		return (AtomicInteger) sc.getAttribute("TOTAL REQUESTS");
	}

}
