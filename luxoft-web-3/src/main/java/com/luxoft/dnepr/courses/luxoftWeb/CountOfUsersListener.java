package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicInteger;

public class CountOfUsersListener implements HttpSessionAttributeListener {

	@Override
	public void attributeAdded(HttpSessionBindingEvent arg0) {

		if(arg0.getName().equals("role")){
			String currentRole = (String) arg0.getValue();
			if(currentRole.equals("admin")){
				getCountOfAdminSession(arg0).getAndIncrement();

			}
			else{
				getCountOfUserSession(arg0).getAndIncrement();
			}
		}

	}

	private AtomicInteger getCountOfAdminSession(HttpSessionBindingEvent arg0){
		return (AtomicInteger)(arg0.getSession().getServletContext().getAttribute("COUNT SESSIONS OF ADMINS"));
	}

	private AtomicInteger getCountOfUserSession(HttpSessionBindingEvent arg0){
		return (AtomicInteger)(arg0.getSession().getServletContext().getAttribute("COUNT SESSIONS OF USERS"));
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub

	}


}
