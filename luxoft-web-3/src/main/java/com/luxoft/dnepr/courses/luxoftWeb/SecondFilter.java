package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SecondFilter implements Filter {

	FilterConfig config;
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		if (login == null) {
			request.getSession().invalidate();
			response.sendRedirect("index.jsp");
		} else {

			if (MapOfAllUsers.getMapOfUsers().containsKey(login) && MapOfAllUsers.getMapOfUsers().get(login).get(0).equals(password)) {
				request.getSession().setAttribute("login", login);
				String expectedRole = MapOfAllUsers.getMapOfUsers().get(login).get(1);
				request.getSession().setAttribute("role", expectedRole);
				arg2.doFilter(request, response);
			} else {

				request.getSession().invalidate();
				RequestDispatcher rd =config.getServletContext().getRequestDispatcher("/index.jsp");
				PrintWriter out= response.getWriter();
				out.println("<font color=red>Wrong login or password</font>");
				rd.include(request, response);

			}
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		config=arg0;

	}

}
