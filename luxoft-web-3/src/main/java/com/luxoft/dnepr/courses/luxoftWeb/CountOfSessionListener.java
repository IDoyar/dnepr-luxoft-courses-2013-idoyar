package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicInteger;

public class CountOfSessionListener implements HttpSessionListener{

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		countOfTotalSessions(arg0).getAndIncrement();
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
				
	}
	
	private AtomicInteger countOfTotalSessions(HttpSessionEvent arg0){
		
		return (AtomicInteger)arg0.getSession().getServletContext().getAttribute("TOTAL SESSIONS");
	}

}
