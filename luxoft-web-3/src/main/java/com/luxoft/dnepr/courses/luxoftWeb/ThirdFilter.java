package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ThirdFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) arg0;
		HttpServletResponse res = (HttpServletResponse) arg1;
		HttpSession session = req.getSession(false);

		String currentRole = null;
		if (session == null) {
			res.sendRedirect(req.getContextPath() + "/index.jsp");

		} else
			currentRole = MapOfAllUsers.getMapOfUsers().get((String) session.getAttribute("login")).get(1);
		if ("user".equals(currentRole)) {
			res.sendRedirect(req.getContextPath() + "/user");
		} else {
			arg2.doFilter(req, res);
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
