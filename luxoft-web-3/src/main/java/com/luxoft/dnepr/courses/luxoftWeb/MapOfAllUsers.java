package com.luxoft.dnepr.courses.luxoftWeb;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class MapOfAllUsers implements ServletContextListener {


	private final static String Relative_Path_To_XML ="/META-INF/users.xml";

	private static Map<String, List<String>> mapOfUsers = new HashMap<String,List<String>>();
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		String absolutePathToXML=arg0.getServletContext().getRealPath(Relative_Path_To_XML);
		File fileXML = new File(absolutePathToXML);
		try {
			parseXML(fileXML);
		} catch (ParserConfigurationException | SAXException | IOException e) {

			e.printStackTrace();
		}

		arg0.getServletContext().setAttribute("TOTAL SESSIONS", new AtomicInteger(0));
		arg0.getServletContext().setAttribute("TOTAL REQUESTS", new AtomicInteger(0));
		arg0.getServletContext().setAttribute("COUNT SESSIONS OF ADMINS", new AtomicInteger(0));
		arg0.getServletContext().setAttribute("COUNT SESSIONS OF USERS", new AtomicInteger(0));

	}

	private static void parseXML(File file) throws ParserConfigurationException, SAXException, IOException{



		DocumentBuilderFactory factory =

				DocumentBuilderFactory.newInstance();

		//Get the DOM Builder

		DocumentBuilder builder = factory.newDocumentBuilder();

		//Load and Parse the XML document

		//document contains the complete XML as a Tree.

		Document document =

				builder.parse(file);

		//Iterating through the nodes and extracting the data.

		NodeList nodeList = document.getDocumentElement().getElementsByTagName("user");

		for (int i = 0; i < nodeList.getLength(); i++) {

			List<String> usersAttr = new ArrayList<String>();

			//We have encountered an <employee> tag.

			Node node = (Node) nodeList.item(i);

			String name = node.getAttributes().

					getNamedItem("name").getNodeValue();

			String password = node.getAttributes().

					getNamedItem("password").getNodeValue();

			String role = node.getAttributes().

					getNamedItem("role").getNodeValue();

			usersAttr.add(password);

			usersAttr.add(role);

			mapOfUsers.put(name, usersAttr);

		}
	}

	public static Map<String, List<String>> getMapOfUsers() {
		return mapOfUsers;
	}

}
