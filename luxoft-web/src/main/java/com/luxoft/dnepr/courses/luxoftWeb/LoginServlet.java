package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;

public class LoginServlet extends DispatcherServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServerException, ServletException{

        String login = request.getParameter("login");
        String password = request.getParameter("password");

        log("login="+login+"::password="+password);

        String expectedPassword = MapOfAllUsers.getMapOfUsers().get(login);
        if(expectedPassword!=null && expectedPassword.equals(password)){// has such login in users.xml
            request.getSession().setAttribute("login", login);
            super.forward("/WEB-INF/registration.jsp",request, response);

        }

        else{
            request.getSession().invalidate();
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.html");
            PrintWriter out= response.getWriter();
            out.println("<font color=red>Wrong login or password</font>");
            rd.include(request, response);
        }



    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServerException, ServletException{

        request.getSession().invalidate();
        response.sendRedirect("index.html");

    }
}

