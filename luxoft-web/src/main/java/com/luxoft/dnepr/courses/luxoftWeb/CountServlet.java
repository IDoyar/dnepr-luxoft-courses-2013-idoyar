package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CountServlet extends HttpServlet {
    private int counts = 0;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder sb = new StringBuilder();

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");

        counts++;
        sb.append("{\"counts\": ");
        sb.append(counts);
        sb.append("}");

        PrintWriter writer = response.getWriter();
        writer.write(sb.toString());

        response.setContentLength(sb.length());
    }
}
