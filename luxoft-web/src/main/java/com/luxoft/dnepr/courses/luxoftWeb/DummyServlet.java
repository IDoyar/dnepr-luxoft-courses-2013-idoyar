package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DummyServlet extends HttpServlet {

    private Map<String, String> people = new HashMap<String, String>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String jb = getString(request);
        String name = extractFromJson(jb).getName();
        String age = extractFromJson(jb).getAge();
        StringBuilder stringBuilder = new StringBuilder();

        if (name == null || age == null) {

            writer(response);

            stringBuilder.append("{\"error\": \"Illegal parameters\"}");

            setter(response, stringBuilder);

        } else if (people.containsKey(name)) {

            writer(response);

            stringBuilder.append("{\"error\": \"Name ");

            stringBuilder.append(name);

            stringBuilder.append(" already exists\"}");

            setter(response, stringBuilder);

        } else {

            response.setStatus(201);

            people.put(name, age);
        }

    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jb = getString(request);
        String name = extractFromJson(jb).getName();
        String age = extractFromJson(jb).getAge();
        StringBuilder stringBuilder = new StringBuilder();

        if (name == null || age == null) {

            writer(response);

            stringBuilder.append("{\"error\": \"Illegal parameters\"}");

            setter(response, stringBuilder);

        } else if (!people.containsKey(name)) {

            writer(response);

            stringBuilder.append("{\"error\": \"Name ");

            stringBuilder.append(name);

            stringBuilder.append(" does not exist\"}");

            setter(response, stringBuilder);

        } else {

            response.setStatus(202);

            people.put(name, age);
        }

    }

    private String getString(HttpServletRequest request) {
        StringBuilder jb = new StringBuilder();
        String line = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) { /*report an error*/ }
        return jb.toString();
    }

    private static People extractFromJson(String s) {
        Gson g = new Gson();
        People one = g.fromJson(s, People.class);
        return one;
    }

    private static void writer(HttpServletResponse response) {
        response.setStatus(500);
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
    }

    private static void setter(HttpServletResponse response, StringBuilder stringBuilder) throws IOException {
        response.setContentLength(stringBuilder.length());
        response.getWriter().write(stringBuilder.toString());
    }

    private static class People {
        private String age;
        private String name;

        public String getAge() {
            return age;
        }

        public String getName() {
            return name;
        }

    }
}
