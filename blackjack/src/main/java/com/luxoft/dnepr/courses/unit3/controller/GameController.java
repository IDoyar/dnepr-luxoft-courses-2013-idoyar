package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class GameController {
    private static GameController controller;
    private List<Card> list;
    private List<Card> listOfMyHand;
    private List<Card> listOfDealerHand;

    private GameController() {
        listOfMyHand = new ArrayList<Card>();
        listOfDealerHand = new ArrayList<Card>();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler
     */
    void newGame(Shuffler shuffler) {
        list = Deck.createDeck(1);
        shuffler.shuffle(list);
        listOfMyHand.clear();
        listOfDealerHand.clear();
        getInstance().putMyHand();
        getInstance().putMyHand();
        getInstance().putDealersHand();
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        if ((Deck.costOf(getInstance().getMyHand()) > 21) || (list.isEmpty()))
            return false;
        else getInstance().putMyHand();
        if (Deck.costOf(getInstance().getMyHand()) <= 21)
            return true;
        else
            return false;
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        while (Deck.costOf(getInstance().getDealersHand()) < 17)
            getInstance().putDealersHand();
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        if (Deck.costOf(getInstance().getMyHand()) > 21)
            return WinState.LOOSE;
        if ((Deck.costOf(getInstance().getMyHand()) < Deck.costOf(getInstance().getDealersHand()) && (Deck.costOf(getInstance().getDealersHand()) <= 21)))
            return WinState.LOOSE;
        if (Deck.costOf(getInstance().getMyHand()) == Deck.costOf(getInstance().getDealersHand()))
            return WinState.PUSH;
        return WinState.WIN;
    }

    public void putMyHand() {
        listOfMyHand.add(list.get(0));
        list.remove(0);
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        return listOfMyHand;
    }

    public void putDealersHand() {
        listOfDealerHand.add(list.get(0));
        list.remove(0);
    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {
        return listOfDealerHand;
    }
}
