package com.luxoft.dnepr.courses.unit3.view;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class CommandLineInterface {

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: (TODO)\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();

        controller.newGame();

        output.println();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * <p/>
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     * @see com.luxoft.dnepr.courses.unit3.view.CommandLineInterfaceTest
     *      <p/>
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     *      после того как диллер сыграл напечатать:
     *      Dealer turn:
     *      пустая строка
     *      состояние
     *      пустая строка
     *      финальное сообщение
     *      Command.EXIT - выйти из игры
     *      <p/>
     *      Состояние:
     *      рука игрока (total вес)
     *      рука диллера (total вес)
     *      <p/>
     *      например:
     *      3 J 8 (total 21)
     *      A (total 11)
     *      <p/>
     *      Финальное сообщение:
     *      В зависимости от состояния печатаем:
     *      Congrats! You win!
     *      Push. Everybody has equal amount of points.
     *      Sorry, today is not your day. You loose.
     *      <p/>
     *      Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */
    private boolean execute(String command, GameController controller) {
        if (command.equals(Command.HELP)) {
            commandHelp(controller);
            return true;
        }
        if (command.equals(Command.MORE)) {
            if (!controller.requestMore()) {
                commandMore(controller);
                return false;
            } else {
                format(controller.getMyHand());
                format(controller.getDealersHand());
                return true;
            }
        }
        if (command.equals(Command.STOP)) {
            commandStop(controller);
            winState(controller);
            return false;
        }
        if (command.equals(Command.EXIT)) {
            commandExit(controller);
            return false;
        }
        output.println("Invalid command");
        return true;
    }

    private void commandHelp(GameController controller) {
        output.println("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game");
    }

    private void commandMore(GameController controller) {
        format(controller.getMyHand());
        format(controller.getDealersHand());
        output.println();
        output.println("Sorry, today is not your day. You loose.");
    }

    private void commandStop(GameController controller) {
        controller.requestStop();
        output.println("Dealer turn:");
        output.println();
        format(controller.getMyHand());
        format(controller.getDealersHand());
        output.println();
    }

    private void winState(GameController controller) {
        if (controller.getWinState() == WinState.LOOSE) {
            output.println("Sorry, today is not your day. You loose.");
        } else if (controller.getWinState() == WinState.PUSH) {
            output.println("Push. Everybody has equal amount of points.");
        } else {
            output.println("Congrats! You win!");
        }
    }

    private void commandExit(GameController controller) {
        output.println("Good Bye");
    }

    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();
        format(myHand);
        List<Card> dealersHand = controller.getDealersHand();
        format(dealersHand);
    }

    private void format(List<Card> list) {

        for (Card i : list) {
            output.print(i.getRank().getName() + " ");
        }
        output.print("(total " + Deck.costOf(list) + ")");
        output.println();
    }
}