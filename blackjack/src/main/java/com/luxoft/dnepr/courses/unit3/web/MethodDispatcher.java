package com.luxoft.dnepr.courses.unit3.web;

import java.util.List;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

public class MethodDispatcher {

	/**
	 * 
	 * @param request
	 * @param response
	 * @return response or <code>null</code> if wasn't able to find a method.
	 */
	public Response dispatch(Request request, Response response) {
		String method = request.getParameters().get("method");

        if (method == null) {
			return null;
		}
		
		if(method.equals("newGame")) {
			GameController.getInstance().newGame();
			return gameState(response);
		}
			
		if (method.equals("requestMore")) {
			return requestMore(response);
		}

        if (method.equals("requestStop")) {
            return requestStop(response);
        }

		return null;
	}

	private Response requestMore(Response response) {
		response.write("{\"result\": " + GameController.getInstance().requestMore());
		response.write(", \"myHand\": ");
		List<Card> myHand =  GameController.getInstance().getMyHand();
		writeHand(response, myHand.subList(myHand.size()-1,myHand.size()));
		
		response.write("}");
		
		return response;
	}
	

	private void writeHand(Response response, List<Card> hand) {
		boolean isFirst = true;
		response.write("[");
		for (Card card : hand) {
			if (isFirst) {
				isFirst = false;
			} else {
				response.write(",");
			}
			response.write("{\"rank\": \"");
			response.write(card.getRank().getName());
			response.write("\", \"suit\": \"");
			response.write(card.getSuit().name());
			response.write("\", \"cost\": \"");
			response.write(String.valueOf(card.getCost()));
			response.write("\"}");
		}
		response.write("]");
	}

    private Response requestStop(Response response) {

        GameController.getInstance().requestStop();

        response.write("{\"result\": \""+GameController.getInstance().getWinState());

        response.write("\", \"dealersHand\": ");

        List<Card> dealersHand = GameController.getInstance().getDealersHand();

        writeHand(response, dealersHand.subList(1,dealersHand.size()));

        response.write(", \"winState\": \"" + GameController.getInstance().getWinState());

        response.write("\"}");

        return response;
    }
    
    private Response gameState(Response response) {
        List myHand = GameController.getInstance().getMyHand();
        List dealersHand = GameController.getInstance().getDealersHand();
        response.write("{\"result\": true, \"myHand\": ");
        writeHand(response, myHand);
        response.write(", \"dealersHand\": ");
        writeHand(response, dealersHand);

        response.write("}");

        return response;
    }

}
