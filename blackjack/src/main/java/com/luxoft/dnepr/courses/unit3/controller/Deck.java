package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

public final class Deck {

    private Deck() {
    }

    public static List<Card> createDeck(int size) {
        if (size <= 1)
            return create(1);
        if (size >= 10)
            return create(10);

        return create(size);
    }

    private static List<Card> create(int size) {
        List<Card> deck = new ArrayList<Card>();
        for (int deckN = 0; deckN < size; deckN++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    deck.add(new Card(rank, suit));
                }
            }
        }
        return deck;
    }


    public static int costOf(List<Card> hand) {
        int sum = 0;
        for (int i = 0; i < hand.size(); i++)
            sum += hand.get(i).getCost();
        return sum;
    }
}


