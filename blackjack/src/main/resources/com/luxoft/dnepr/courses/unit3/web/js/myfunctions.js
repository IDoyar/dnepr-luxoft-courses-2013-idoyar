var gameEnd = false;

function NewGame()
{
    gameEnd = false;
	$('#player_hand').empty();
    $('#dealer_hand').empty();
	letsPlay();
}

function letsPlay()
{
		$.ajax({
		url: "http://localhost:8081/service",
		data: {method: 'newGame'}
   	    }).done(function(response) {
		    var json = $.parseJSON(response);
		    giveCards(json.dealersHand,"#dealer_hand");
		    giveCards(json.myHand,"#player_hand");
	    });
}


function hit()
{
	if (gameEnd == false) {
	$.ajax({
   	url: "http://localhost:8081/service",
	data: { method: 'requestMore' }
    }).done(function(response){
		var json = $.parseJSON(response);
		giveCards(json.myHand,"#player_hand");
   		if (json.result === false) {
            gameOver ("LOOSE");
		}
	});
	}
}

function stand()
{
	if (gameEnd == false) {
	$.ajax({
   	url: "http://localhost:8081/service",
	data: { method: 'requestStop' }
   	}).done(function(response){
		var json = $.parseJSON(response);
		giveCards(json.dealersHand,"#dealer_hand");
        gameOver (json.winState);
	});
	}
}

function gameOver (winState) {
    gameEnd = true;
    if ( winState === "WIN") {
        print("YOU WIN");

    } else if (winState === "LOOSE" ) {
    	print("YOU LOOSE");

    } else {
    	print("PUSH");
    }
}


function print(text)
{
     $('<h1>' + text + '</h1>').css({'text-align':'center'}).appendTo($("#dialog"));
}

function giveCards(cards,hand)
{
    for (var i=0;i<cards.length;i++) {
         $('<img src="cards/' + cards[i].rank + cards[i].suit +'.jpg">').css('float','left').appendTo($(hand));
    }
}






