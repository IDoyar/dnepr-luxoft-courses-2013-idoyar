package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;
import com.luxoft.dnepr.courses.unit3.model.WinState;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class GameControllerTest {

    @Before
    // ���������� ����, ������� ������� ���������� ��� ������� ����� (����� ��� �����������)
    public void prepare() throws Exception {
        Field fld = GameController.class.getDeclaredField("controller");
        try {
            fld.setAccessible(true);
            fld.set(null, null);
        } finally {
            fld.setAccessible(false);
        }
    }

    @Test
    public void testGetInstance() {
        assertSame(GameController.getInstance(), GameController.getInstance());
    }

    @Test
    public void testBeforeNewGame() {
        assertTrue(GameController.getInstance().getMyHand().isEmpty());
        assertTrue(GameController.getInstance().getDealersHand().isEmpty());

        assertEquals(WinState.PUSH, GameController.getInstance().getWinState());
    }

    @Test
    public void testAfterNewGame() {
        GameController.getInstance().newGame();
        assertEquals(2, GameController.getInstance().getMyHand().size());
        assertEquals(1, GameController.getInstance().getDealersHand().size());
    }

    @Test
    public void testRequestMoreFirst() {
        GameController.getInstance().newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_ACE, Suit.CLUBS));
                deck.set(1, new Card(Rank.RANK_10, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_JACK, Suit.SPADES));
                deck.set(3, new Card(Rank.RANK_ACE, Suit.HEARTS));
                deck.set(4, new Card(Rank.RANK_2, Suit.DIAMONDS));
                deck.set(5, new Card(Rank.RANK_7, Suit.CLUBS));

            }

        });


        assertEquals(2, GameController.getInstance().getMyHand().size());
        assertEquals(Rank.RANK_ACE, GameController.getInstance().getMyHand().get(0).getRank());
        assertEquals(Rank.RANK_10, GameController.getInstance().getMyHand().get(1).getRank());

        assertEquals(1, GameController.getInstance().getDealersHand().size());
        assertEquals(Rank.RANK_JACK, GameController.getInstance().getDealersHand().get(0).getRank());

        assertEquals(WinState.WIN, GameController.getInstance().getWinState());

        GameController.getInstance().requestMore();

        // И так на руках 21 очко, поэтому какая бы карта не попалась сумма будет больше 21, а значит проигрыш

        assertEquals(WinState.LOOSE, GameController.getInstance().getWinState());

    }

    @Test
    public void testRequestMoreSecond() {
        GameController.getInstance().newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_ACE, Suit.CLUBS));
                deck.set(1, new Card(Rank.RANK_10, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_JACK, Suit.SPADES));

            }

        });


        assertEquals(2, GameController.getInstance().getMyHand().size());
        assertEquals(Rank.RANK_ACE, GameController.getInstance().getMyHand().get(0).getRank());
        assertEquals(Rank.RANK_10, GameController.getInstance().getMyHand().get(1).getRank());

        assertEquals(1, GameController.getInstance().getDealersHand().size());
        assertEquals(Rank.RANK_JACK, GameController.getInstance().getDealersHand().get(0).getRank());

        assertEquals(WinState.WIN, GameController.getInstance().getWinState());

        GameController.getInstance().requestMore();

        // Колода пуста, поэтому должно выдать LOOSE

        assertEquals(WinState.LOOSE, GameController.getInstance().getWinState());

    }

    @Test
    public void testRequestMoreThird() {
        GameController.getInstance().newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_ACE, Suit.CLUBS));
                deck.set(1, new Card(Rank.RANK_ACE, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_JACK, Suit.SPADES));
                deck.set(3, new Card(Rank.RANK_ACE, Suit.HEARTS));
                deck.set(4, new Card(Rank.RANK_2, Suit.DIAMONDS));
                deck.set(5, new Card(Rank.RANK_7, Suit.CLUBS));

            }

        });


        assertEquals(2, GameController.getInstance().getMyHand().size());
        assertEquals(Rank.RANK_ACE, GameController.getInstance().getMyHand().get(0).getRank());
        assertEquals(Rank.RANK_ACE, GameController.getInstance().getMyHand().get(1).getRank());

        assertEquals(1, GameController.getInstance().getDealersHand().size());
        assertEquals(Rank.RANK_JACK, GameController.getInstance().getDealersHand().get(0).getRank());

        // Сразу у игрока 22 очка, поэтому он проиграл

        assertEquals(WinState.LOOSE, GameController.getInstance().getWinState());

    }

    @Test
    public void testRequestStopFirst() {

        GameController.getInstance().newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_ACE, Suit.CLUBS));
                deck.set(1, new Card(Rank.RANK_10, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_JACK, Suit.SPADES));
                deck.set(3, new Card(Rank.RANK_ACE, Suit.HEARTS));
                deck.set(4, new Card(Rank.RANK_2, Suit.DIAMONDS));
                deck.set(5, new Card(Rank.RANK_7, Suit.CLUBS));
            }

        });
        assertEquals(2, GameController.getInstance().getMyHand().size());
        assertEquals(Rank.RANK_ACE, GameController.getInstance().getMyHand().get(0).getRank());
        assertEquals(Rank.RANK_10, GameController.getInstance().getMyHand().get(1).getRank());

        assertEquals(1, GameController.getInstance().getDealersHand().size());
        assertEquals(Rank.RANK_JACK, GameController.getInstance().getDealersHand().get(0).getRank());
        assertEquals(WinState.WIN, GameController.getInstance().getWinState());

        GameController.getInstance().requestStop();

        // В данном случае будет ничья, хоть у игрока на руках и 21 очко, но в колоде след. карта туз, и дилер наберет тоже 21 очко

        assertEquals(WinState.PUSH, GameController.getInstance().getWinState());

        assertTrue(Deck.costOf(GameController.getInstance().getDealersHand()) == 21);
    }

    @Test
    public void testRequestStopSecond() {

        GameController.getInstance().newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_ACE, Suit.CLUBS));
                deck.set(1, new Card(Rank.RANK_10, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_JACK, Suit.SPADES));
                deck.set(3, new Card(Rank.RANK_8, Suit.HEARTS));
                deck.set(4, new Card(Rank.RANK_2, Suit.DIAMONDS));
                deck.set(5, new Card(Rank.RANK_7, Suit.CLUBS));
            }

        });
        assertEquals(2, GameController.getInstance().getMyHand().size());
        assertEquals(Rank.RANK_ACE, GameController.getInstance().getMyHand().get(0).getRank());
        assertEquals(Rank.RANK_10, GameController.getInstance().getMyHand().get(1).getRank());

        assertEquals(1, GameController.getInstance().getDealersHand().size());
        assertEquals(Rank.RANK_JACK, GameController.getInstance().getDealersHand().get(0).getRank());
        assertEquals(WinState.WIN, GameController.getInstance().getWinState());

        GameController.getInstance().requestStop();

        // В данном случае игрок выиграет, поскольку дилер доберет еще одну карту и получит 18 очков

        assertEquals(WinState.WIN, GameController.getInstance().getWinState());

        assertTrue(Deck.costOf(GameController.getInstance().getDealersHand()) == 18);
    }
}
