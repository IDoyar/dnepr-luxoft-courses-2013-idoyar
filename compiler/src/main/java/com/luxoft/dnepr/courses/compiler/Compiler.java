package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class Compiler {

    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        if (input == null || input.isEmpty()) {
            throw new CompilationException("input isEmpty or null");
        }
        String newInput = input.replaceAll(" ", "");
        StringTokenizer tok = new StringTokenizer(newInput, "+-*/", true);

//        String firstDouble = new String();
//        String secondDouble = new String();
//        String firstSign = new String();
//        String secondSign = new String();
//        String thirdSign = new String();

        String firstDouble = null;
        String secondDouble = null;
        String firstSign = null;
        String secondSign = null;
        String thirdSign = null;



        StringBuilder firstArg = new StringBuilder();
        StringBuilder sign = new StringBuilder();
        StringBuilder secondArg = new StringBuilder();

        StringBuilder firstNegValue = new StringBuilder();
        StringBuilder secondNegValue = new StringBuilder();

        if (tok.countTokens() == 3) {
            parseInputWithThreeSymbols(firstDouble, secondDouble, firstSign, tok, firstArg, sign, secondArg);
        } else {
            if (tok.countTokens() == 4) {
                parseInputWithFourSymbols(firstDouble, secondDouble, firstSign, secondSign, tok, firstArg, sign, secondArg, firstNegValue);
            } else {
                if (tok.countTokens() == 5) {
                    parseInputWithFiveSymbols(firstDouble, secondDouble, firstSign, secondSign, thirdSign, tok, firstArg, sign, secondArg, firstNegValue, secondNegValue);
                } else {
                    throw new CompilationException("Uncorrectly input");
                }
            }
        }

        addCommandToBuffer(result, firstArg, secondArg, sign);

        return result.toByteArray();
    }

    private static void addCommandToBuffer(ByteArrayOutputStream result, StringBuilder firstArg, StringBuilder secondArg, StringBuilder sign) {
        addCommand(result, VirtualMachine.PUSH, Double.valueOf(firstArg.toString()));
        addCommand(result, VirtualMachine.PUSH, Double.valueOf(secondArg.toString()));
        addCommand(result, sign.charAt(0));
        addCommand(result, VirtualMachine.PRINT);
    }


    private static void parseInputWithThreeSymbols(String firstDouble, String secondDouble, String firstSign, StringTokenizer tok, StringBuilder firstArg, StringBuilder sign, StringBuilder secondArg) {

        firstDouble = tok.nextToken();
        firstSign = tok.nextToken();
        secondDouble = tok.nextToken();
        defineArgs(firstArg, sign, secondArg, firstDouble, secondDouble, firstSign);

    }


    private static void parseInputWithFourSymbols(String firstDouble, String secondDouble, String firstSign, String secondSign, StringTokenizer tok, StringBuilder firstArg, StringBuilder sign, StringBuilder secondArg, StringBuilder firstNegValue) {

        String first = tok.nextToken();

        if (assertIsNumber(first)) {
            firstDouble = first;
            firstSign = tok.nextToken();
            firstNegValue.append(tok.nextToken());
            firstNegValue.append(tok.nextToken());
            secondDouble = firstNegValue.toString();
            defineArgs(firstArg, sign, secondArg, firstDouble, secondDouble, firstSign);
        } else {
            if (assertIsSign(first)) {
                firstNegValue.append(first);
                firstNegValue.append(tok.nextToken());
                firstDouble = firstNegValue.toString();
                firstSign = tok.nextToken();
                secondDouble = tok.nextToken();
                defineArgs(firstArg, sign, secondArg, firstDouble, secondDouble, firstSign);
            } else {
                throw new CompilationException("Uncorrectly input");
            }
        }

    }

    private static void parseInputWithFiveSymbols(String firstDouble, String secondDouble, String firstSign, String secondSign, String thirdSign, StringTokenizer tok, StringBuilder firstArg, StringBuilder sign, StringBuilder secondArg, StringBuilder firstNegValue, StringBuilder secondNegValue) {

        String first = tok.nextToken();

        if (assertIsSign(first)) {

            firstNegValue.append(first);
            firstNegValue.append(tok.nextToken());

            firstDouble = firstNegValue.toString();
            firstSign = tok.nextToken();

            secondNegValue.append(tok.nextToken());
            secondNegValue.append(tok.nextToken());

            secondDouble = secondNegValue.toString();
            defineArgs(firstArg, sign, secondArg, firstDouble, secondDouble, firstSign);
        } else {
            throw new CompilationException("Uncorrectly input");
        }
    }

    private static void defineArgs(StringBuilder firstArg, StringBuilder sign, StringBuilder secondArg, String firstDouble, String secondDouble, String firstSign) {

        if (assertIsNumber(firstDouble)) {
            firstArg.append(firstDouble);
        } else {
            throw new CompilationException("Uncorrectly value");
        }

        if (assertIsSign(firstSign)) {
            sign.append(firstSign);
        } else {
            throw new CompilationException("Uncorrectly sign");
        }

        if (assertIsNumber(secondDouble)) {
            secondArg.append(secondDouble);
        } else {
            throw new CompilationException("Uncorrectly value");
        }
    }


    private static void addCommand(ByteArrayOutputStream result, char sign) {
        switch (sign) {
            case '+':
                addCommand(result, VirtualMachine.ADD);
                break;
            case '-': {
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.SUB);
                break;
            }
            case '*':
                addCommand(result, VirtualMachine.MUL);
                break;
            case '/': {
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.DIV);
                break;
            }
            default: {

            }

        }
    }

    private static boolean assertIsNumber(String token) {
        try {
            Double.valueOf(token);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private static boolean assertIsSign(String token) {
        if (token.length() != 1)
            return false;
        char symb = token.charAt(0);
        return symb == '+' || symb == '-' || symb == '*' || symb == '/';
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
//		while (scanner.hasNext()) {
//			data.add(scanner.next());
//		}
        data.add(scanner.nextLine());
        scanner.close();
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }
}

//    public static final boolean DEBUG = true;
//
//    public static void main(String[] args) {
//        byte[] byteCode = compile(getInputString(args));
//        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
//        vm.run();
//    }
//
//    private static ByteArrayOutputStream writeToBuffer(List<Operation> operations){
//        ByteArrayOutputStream result = new ByteArrayOutputStream();
//        for (int i = 0;i < operations.size(); i++){
//            Operation operation = operations.get(i);
//            if (i > 0)
//                addCommand(result, VirtualMachine.POP);
//
//            //We execute PUSH in reverse order instead of PUSH and SWAP in the cases DIV and SUB
//            addCommand(result, VirtualMachine.PUSH, operation.getB());
//            addCommand(result, VirtualMachine.PUSH, operation.getA());
//            addCommand(result, operation.getOperationByteCode());
//        }
//        addCommand(result, VirtualMachine.PRINT);
//        return result;
//    }
//
//    static byte[] compile(String input) {
//
//        if(input==null || input.isEmpty()){
//            //assert false;
//            throw new CompilationException("Uncorrectly input string - null or empty");
//        }
//        String newInput = input.replaceAll(" ", "");
//        List<Operation> operations = computeOperationsInLineWithBrackets(newInput);
//        ByteArrayOutputStream result = writeToBuffer(operations);
//        return result.toByteArray();
//    }
//
//    private static List<Operation> computeOperationsInLineWithBrackets(String input){
//        List<Operation> operations = new ArrayList<>();
//        StringBuilder changeInput = new StringBuilder(input);
//        int indexCloseBracket;
//        while((indexCloseBracket = changeInput.indexOf(")"))!=-1){
//            int indexOpenBracket = changeInput.lastIndexOf("(", indexCloseBracket);
//            if(indexOpenBracket==-1){
//                //assert false;
//                throw new CompilationException("Uncorrectly input string - ( is absent");
//            }
//            String substring = changeInput.substring(indexOpenBracket+1, indexCloseBracket);
//            operations.addAll(computeOperationsInLineWithoutBrackets(substring));
//            changeInput.delete(indexOpenBracket, indexCloseBracket+1);
//            changeInput.insert(indexOpenBracket, computeValueInLineWithoutBrackets(substring));
//        }
//        operations.addAll(computeOperationsInLineWithoutBrackets(changeInput.toString()));
//        return operations;
//    }
//
//    private static List<Operation> computeOperationsInLineWithoutBrackets(String input){
//        List<String> numbers = new ArrayList<>();
//        StringBuilder signs = new StringBuilder();
//        parseStringWithoutBrackets(input, numbers, signs);
//        List<Operation> res = new ArrayList<>();
//        while(signs.length() > 0){
//            Operation next = executeCommandInStringWithoutBrackets(numbers, signs);
//            res.add(next);
//        }
//        return res;
//    }
//
//    private static String computeValueInLineWithoutBrackets(String input){
//        List<String> numbers = new ArrayList<>();
//        StringBuilder signs = new StringBuilder();
//        parseStringWithoutBrackets(input, numbers, signs);
//        while(signs.length() > 0){
//            executeCommandInStringWithoutBrackets(numbers, signs);
//        }
//        return numbers.get(0);
//    }
//
//    /**
//     * We save the next operation and changes the numbers and signs after that
//     *
//     * @param numbers
//     * @param signs
//     * @return
//     */
//    private static Operation executeCommandInStringWithoutBrackets(List<String> numbers, StringBuilder signs){
//
//        int executionPosition = findHigestPriorityOperationIndex(signs);
//
//        Operation operation = new Operation(numbers.get(executionPosition),
//                numbers.get(executionPosition + 1),  signs.charAt(executionPosition));
//        double result = operation.getValue();
//        numbers.remove(executionPosition+1);
//        numbers.set(executionPosition, String.valueOf(result));
//        signs.deleteCharAt(executionPosition);
//        return operation;
//    }
//
//    private static int findHigestPriorityOperationIndex(StringBuilder signs) {
//        int executionPosition;
//        int multPosition = signs.indexOf("*");
//        int divPosition = signs.indexOf("/");
//        multPosition = multPosition == -1 ? signs.length() : multPosition;
//        divPosition = divPosition == -1 ? signs.length() : divPosition;
//        executionPosition = Math.min(multPosition, divPosition);
//        if (executionPosition == signs.length()) // no multiplication and division signs
//            executionPosition = 0;
//        return executionPosition;
//    }
//
//    /**
//     * We write to variable numbers all numbers of input, and write to variable signs all signs of input.
//     */
//    private static void parseStringWithoutBrackets(String input, List<String> numbers, StringBuilder signs ){
//        assert input.indexOf(' ') == -1;
//        assert input.indexOf('(') == -1;
//        assert input.indexOf(')') == -1;
//        StringTokenizer tokens = new StringTokenizer(input, "+-*/", true);
//        //addToNumbersAndSigns(numbers,signs,tokens);
//        int i = 0;
//        while(tokens.hasMoreTokens()){
//            String token = tokens.nextToken();
//            if(i % 2 == 0){
//                assert assertIsNumber(token);
//                numbers.add(token);
//            } else {
//                assert assertIsSign(token);
//                signs.append(token);
//            }
//            i++;
//        }
//    }
//

//
//    private static boolean assertIsNumber(String token){
//        try{
//            Double.valueOf(token);
//        }
//        catch(NumberFormatException e){
//            //assert false;
//            return false;
//        }
//        return true;
//    }
//    private static boolean assertIsSign(String token){
//        if (token.length() != 1)
//            return false;
//        char symb = token.charAt(0);
//        return symb == '+' || symb == '-' || symb == '*' || symb == '/';
//    }
//
//    /**
//     * Adds specific command to the byte stream.
//     *
//     * @param result
//     * @param command
//     */
//    public static void addCommand(ByteArrayOutputStream result, byte command) {
//        result.write(command);
//    }
//
//    /**
//     * Adds specific command with double parameter to the byte stream.
//     *
//     * @param result
//     * @param command
//     * @param value
//     */
//    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
//        result.write(command);
//        writeDouble(result, value);
//    }
//
//    private static void writeDouble(ByteArrayOutputStream result, double val) {
//        long bits = Double.doubleToLongBits(val);
//
//        result.write((byte) (bits >>> 56));
//        result.write((byte) (bits >>> 48));
//        result.write((byte) (bits >>> 40));
//        result.write((byte) (bits >>> 32));
//        result.write((byte) (bits >>> 24));
//        result.write((byte) (bits >>> 16));
//        result.write((byte) (bits >>> 8));
//        result.write((byte) (bits >>> 0));
//    }
//
//    private static String getInputString(String[] args) {
//        if (args.length > 0) {
//            return join(Arrays.asList(args));
//        }
//
//        Scanner scanner = new Scanner(System.in);
//        List<String> data = new ArrayList<String>();
//
//        // Use the method scanner.nextLine() instead of
//        //while (scanner.hasNext()) {
//        // data.add(scanner.next());}
//        // in order to testing the program using the IDE (not Command line)
//        data.add(scanner.nextLine());
//        scanner.close();
//        return join(data);
//    }
//
//    private static String join(List<String> list) {
//        StringBuilder result = new StringBuilder();
//        for (String element : list) {
//            result.append(element);
//        }
//        return result.toString();
//    }
//
//    public static class Operation{
//        private double _a;
//        private double _b;
//        private char _sign;
//
//        public byte getOperationByteCode(){
//            switch(_sign){
//                case '+':
//                    return VirtualMachine.ADD;
//                case '-':
//                    return VirtualMachine.SUB;
//                case '*':
//                    return VirtualMachine.MUL;
//                case '/':
//                    return VirtualMachine.DIV;
//                default:
//                {
//                    assert false;
//                    return 0x10;
//                }
//            }
//        }
//
//        public double getValue(){
//            switch(_sign){
//                case '+':
//                    return _a + _b;
//                case '-':
//                    return _a - _b;
//                case '*':
//                    return _a * _b;
//                case '/':
//                    return _a / _b;
//                default:
//                {
//                    assert false;
//                    return Double.NaN;
//                }
//            }
//        }
//
//        public Operation(String a,String b,char c){
//            _a = Double.valueOf(a);
//            _b = Double.valueOf(b);
//            _sign = c;
//        }
//
//        public double getA() {
//            return _a;
//        }
//
//        public double getB() {
//            return _b;
//        }
//
//        public char getSign() {
//            return _sign;
//        }
//
//    }


//}
